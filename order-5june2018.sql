-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 07:36 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `order`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(17, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 09:36:46', '2018-05-31 09:36:46'),
(18, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 09:37:00', '2018-05-31 09:37:00'),
(19, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 10:07:52', '2018-05-31 10:07:52'),
(20, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 10:08:05', '2018-05-31 10:08:05'),
(21, 1, 1, 4, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-05-31 10:27:14', '2018-05-31 10:27:14'),
(22, 1, 1, 4, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-05-31 10:27:47', '2018-05-31 10:27:47'),
(23, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:04:00', '2018-06-02 00:04:00'),
(24, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:07:11', '2018-06-02 00:07:11'),
(25, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:07:20', '2018-06-02 00:07:20'),
(26, 1, 1, 4, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-02 00:07:46', '2018-06-02 00:07:46'),
(27, 1, 1, 4, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-02 00:13:03', '2018-06-02 00:13:03'),
(28, 1, 1, 4, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-02 00:13:26', '2018-06-02 00:13:26'),
(29, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:13:43', '2018-06-02 00:13:43'),
(30, 1, 1, 4, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-02 00:25:16', '2018-06-02 00:25:16'),
(31, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:37:35', '2018-06-02 00:37:35'),
(32, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:41:09', '2018-06-02 00:41:09'),
(33, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:41:29', '2018-06-02 00:41:29'),
(34, 1, 1, 2, 'save', 'bg-aqua', 'trans("history.backend.users.updated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:48:18', '2018-06-02 00:48:18'),
(35, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-06-02 00:57:13', '2018-06-02 00:57:13'),
(36, 1, 1, 4, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-03 00:22:14', '2018-06-03 00:22:14'),
(37, 1, 1, 4, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-06-03 00:22:44', '2018-06-03 00:22:44'),
(38, 1, 1, 1, 'save', 'bg-aqua', 'trans("history.backend.users.updated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","Admin",1]}', '2018-06-03 10:43:51', '2018-06-03 10:43:51'),
(39, 1, 1, 5, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","yojan shresa",5]}', '2018-06-04 11:24:24', '2018-06-04 11:24:24'),
(40, 1, 1, 5, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","yojan shresthaasdf",5]}', '2018-06-04 11:35:13', '2018-06-04 11:35:13'),
(41, 1, 1, 5, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","yojan shresthaasdf",5]}', '2018-06-04 11:37:17', '2018-06-04 11:37:17'),
(42, 1, 1, 5, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","yojan Shrestha",5]}', '2018-06-04 11:45:41', '2018-06-04 11:45:41'),
(43, 1, 1, 5, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","yojan Shrestha",5]}', '2018-06-04 11:45:56', '2018-06-04 11:45:56'),
(44, 1, 1, 8, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",8]}', '2018-06-04 18:59:46', '2018-06-04 18:59:46'),
(45, 1, 1, 8, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",8]}', '2018-06-04 18:59:57', '2018-06-04 18:59:57'),
(46, 1, 1, 8, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",8]}', '2018-06-04 19:00:08', '2018-06-04 19:00:08'),
(47, 1, 1, 8, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",8]}', '2018-06-04 19:00:18', '2018-06-04 19:00:18'),
(48, 1, 1, 9, 'plus', 'bg-green', 'trans("history.backend.users.created") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","test5",9]}', '2018-06-04 19:02:09', '2018-06-04 19:02:09'),
(49, 1, 1, 9, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","test5",9]}', '2018-06-04 19:02:25', '2018-06-04 19:02:25'),
(50, 1, 1, 6, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","sajan shresh",6]}', '2018-06-04 19:04:38', '2018-06-04 19:04:38'),
(51, 1, 1, 6, 'trash', 'bg-maroon', 'trans("history.backend.users.deleted") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","sajan shrestha",6]}', '2018-06-04 19:56:53', '2018-06-04 19:56:53'),
(52, 1, 1, 6, 'trash', 'bg-maroon', 'trans("history.backend.users.permanently_deleted") <strong>{user}</strong>', NULL, '2018-06-04 19:58:35', '2018-06-04 19:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2017-04-18 01:44:05', '2017-04-18 01:44:05'),
(2, 'Role', '2017-04-18 01:44:05', '2017-04-18 01:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `status` varchar(30) NOT NULL,
  `order_cancel` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `identifier`, `user_id`, `sub_total`, `total`, `status`, `order_cancel`, `created_at`, `updated_at`) VALUES
(1, 'xip0zIoakwBmm5w8', 4, '600.00', '600.00', 'verified', 0, '2018-06-05 00:43:46', '2018-06-04 18:58:46'),
(2, 'UGIoLygrVJsnEtVY', 5, '300300.00', '300300.00', 'pending', 0, '2018-06-04 11:41:03', '2018-06-04 11:41:03'),
(3, 'NJZg0Hljjgh111UL', 5, '300300.00', '300300.00', 'pending', 0, '2018-06-04 11:48:23', '2018-06-04 11:48:23'),
(4, 'hXNRtZPuGZj8GKeN', 5, '600600.00', '600600.00', 'pending', 0, '2018-06-04 12:33:45', '2018-06-04 12:33:45'),
(5, 'zxbVewYUpMnHESWZ', 6, '46.48', '46.48', 'cancelled', 1, '2018-06-05 01:31:26', '2018-06-04 19:46:26'),
(6, 'MpkgiO20ZwS6Sht3', 6, '946.48', '946.48', 'verified', 0, '2018-06-05 01:35:48', '2018-06-04 19:50:48');

-- --------------------------------------------------------

--
-- Table structure for table `order_cancelled`
--

CREATE TABLE `order_cancelled` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_cancelled`
--

INSERT INTO `order_cancelled` (`id`, `order_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '2018-06-03 09:27:24', '2018-06-03 09:27:24'),
(2, 5, 6, '2018-06-04 19:21:58', '2018-06-04 19:21:58'),
(3, 6, 6, '2018-06-04 19:45:04', '2018-06-04 19:45:04'),
(4, 6, 6, '2018-06-04 19:47:09', '2018-06-04 19:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `qty`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, '300.00', '2018-06-03 01:00:55', '2018-06-03 01:00:55'),
(2, 2, 1, 1, '300.00', '2018-06-04 11:41:03', '2018-06-04 11:41:03'),
(3, 2, 2, 1, '300000.00', '2018-06-04 11:41:03', '2018-06-04 11:41:03'),
(4, 3, 1, 1, '300.00', '2018-06-04 11:48:23', '2018-06-04 11:48:23'),
(5, 3, 2, 1, '300000.00', '2018-06-04 11:48:23', '2018-06-04 11:48:23'),
(6, 4, 1, 2, '300.00', '2018-06-04 12:33:45', '2018-06-04 12:33:45'),
(7, 4, 2, 2, '300000.00', '2018-06-04 12:33:46', '2018-06-04 12:33:46'),
(8, 5, 3, 2, '23.24', '2018-06-04 19:17:36', '2018-06-04 19:17:36'),
(9, 6, 1, 3, '300.00', '2018-06-04 19:28:43', '2018-06-04 19:28:43'),
(10, 6, 3, 2, '23.24', '2018-06-04 19:28:43', '2018-06-04 19:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2017-04-18 01:44:04', '2017-04-18 01:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `feat_img` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `inventory` enum('in stock','out of stock') NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `slug`, `url`, `status`, `feat_img`, `price`, `qty`, `inventory`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Test', 'test', 'http://www.example.com', 1, 'SByo0-2016_2$largeimg125_Feb_2016_004300130.jpg', '300.00', 5, 'in stock', 'lorem ipsum de inc', '2018-06-02 10:11:56', '2018-06-02 10:11:56'),
(2, 1, 'TV', '111', 'http://www.example.com', 1, 'SFWqd-336-280.png', '300000.00', 10, 'in stock', 'coolest HD Quality 33''''', '2018-06-02 11:38:10', '2018-06-02 11:38:10'),
(3, 1, 'speaker phone', 'lkjl', 'kljljl', 1, 'eWfya-mi note 4.jpg', '23.24', 2000, 'in stock', 'jhjhh kjh jkh', '2018-06-05 00:34:52', '2018-06-04 18:49:52'),
(4, 1, 'dsaf', 'dsf', NULL, 0, '', '2.23', 1000, 'in stock', 'asdf', '2018-06-05 00:59:57', '2018-06-04 19:14:57');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `fname`, `lname`, `address`, `phone`, `gender`, `created_at`, `updated_at`) VALUES
(39, 2, 'fname', 'lname', NULL, '1234567890', NULL, '2018-05-30 22:42:25', '2018-05-30 22:42:25'),
(41, 4, 'lakshya', 'maharjan', 'badegau, lalitpur', '9841855515', 'male', '2018-05-31 10:10:28', '2018-05-31 10:10:28'),
(42, 5, 'yojan', 'Shrestha', 'asdfasdfsdf', '98899898989', 'male', '2018-06-04 11:23:21', '2018-06-04 11:23:21'),
(44, 7, 'kjd', 'asdf', 'kljljlkj', '7676767677', 'female', '2018-06-04 17:54:53', '2018-06-04 17:54:53'),
(45, 8, 'fname', 'lname', 'address', '1234567890', 'female', '2018-06-04 17:58:48', '2018-06-04 17:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-04-18 01:44:01', '2017-04-18 01:44:01'),
(2, 'Wholesaler', 0, 4, '2017-06-16 06:11:18', '2017-10-30 02:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(2, 4, 2),
(3, 2, 2),
(4, 1, 1),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('phKsY9MXFGUwoeIrqich2wyLIlThrEuVF0xPlOsU', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'ZXlKcGRpSTZJa2RLWTI0eVlteERaRU5uVHpkaE56bGxYQzlrY25OM1BUMGlMQ0oyWVd4MVpTSTZJa3BwYUc1MlJuUk1jemRjTHpKU1ZGWTJlSHBvV2tweGQxVlNlREZGUnpscE0ycFZNRnd2UjNWMlpXMDFlVlZwTjNsdWNESTVNR3RzZUhablVraFlZbms0UmpkeFRqQmhYQzkxT0NzNFhDOVZUMG94Y0RJMVNXcFBWemR1YTJVd1FsZHFibWRaSzFwMmRXaGlPRE5tVTFKaFVsSmlPRWwwT1VOMVhDOUVSVnBHY2taclVYZHFhbG80VW1SSU55dHpXVEF3YjAxRlZsTTVTMGhwUWsxbGMxYzRPQ3QxWkdSamVrcEVUalZtTUdsa1ZWd3ZZemhoUjFseVRGZGxhekpUYkhkb00ydDVlbUZWZW5sdFVrOVViak51U1hCdlNraHBOM0ZhUmt4QmJDdHJOR1JITkhkYU5IcDRkVlV4WW1oNlYwdEtabGhVYTFSNlUzRlBja0pqU25kUU9XMVpPWE5xZUd0MmQwdGhkSGRIUldONVhDOVBTVmRzTUROeldYTTVkR0pTYWs5VlkxTXJTRTgwT0ZsM2FuVnBLemhDZVRaa2JFMWlaVnBFUVVKblJqQlRURll5TVdwdGNXSlZXVmxLU3pBck5ESklXVkI0V0dKVk9FSmlTMFJCWlVsRVZEVkpOblZZY1ZoUU0wRnZUR0Z2YzBwQlp6WmFhaXRsVkd0dWEzVlFabFZVWkVSVVMyNWtSak42YmswNEt6TnpOMjk0UjI5T1JIVXdlR0ZaVjFsb04wWk5RVmh5VjNKRVpucEhSRFZIY1dKc2N6WnNTbXBUZFdKdFdXMUhkREpUVTBaaldtaDZNMWhtVFRKMWVtWnhjWEE1V0RSRmNYaEpkejA5SWl3aWJXRmpJam9pTnpFMlpHRTFNVEptWkRnNE5XRTJOalV3TURjM1kyWm1abUUzTkRBd016RTJZalppWWpNMFlXVTVOamt5WTJJd05HWmtZMlZrT1Rrd09ESXhNRGMxWVNKOQ==', 1528163187),
('xBoqKc4TddgvH62gr5ZElmt8GXaPGMm9zHjN0BVl', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'ZXlKcGRpSTZJbmxOVUhWSmRVTTNia2RMU2t0R1UybDNhSGRrVjJjOVBTSXNJblpoYkhWbElqb2lZVVJFYmtWNGJqVm1RbkJNVEUxb05XVlRWVXBRVmxveVkyZFNlRXc1UTNGcFZGQTNXWGxHYmpGTlZteE9WRXRaUTJwME56QjVVblJ0WjFsRk16QTFNVk4xTjBkMFRIVkNLM2RpYWxSVE5Fb3plRWhoYWpkb2RrODFTVEJCTXpOa2MxSldUVXhCVG1oV00yUkRkRW93YzJOcWEzQnFZbFZJYzF3dlFrbGhRMHBXTUhWclUzbGpNMEZHWkZkd04wZExaVlJrVUZObmFEbHBSa2wwVW5Sb2QwbzRSSEp4UVhSUlRqbEhUVmRQVFVkYWVrSTFLMEpGYkd4UVZHRTBRVE5jTHpaU1lsWmpaa0ZKTUZCWlNrNTZkbHd2U1ZScFRHdFFNVTVGY0VOUVZURkZiR3BrWXpsTldITjViSFV6V1VKU1YxVktWVWQwYUVoaldGZE5SVUUxYVhvNWVXNUlkVWw0Wms5a1VrOVpaWFV4WlZGRWFVSjFaSGM5UFNJc0ltMWhZeUk2SW1aaVpHRTVNekUzTldVNU1XVXpPRE5oWVRVNU1UVTFPR1ZpT1RjM1pHUXpPVEZqWXpCbVpqZGpPVEk1TkdJNFpUUXdNREV5WXpJMU5USTNZbU14WXpnaWZRPT0=', 1528164097),
('ytSD2I2TfWNnKiMYtKU50heh5HWrvhNg1NS3sibT', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'ZXlKcGRpSTZJa2RFWlN0VVUwNWtSRWhWY201WE5sd3ZOamxOWjJwQlBUMGlMQ0oyWVd4MVpTSTZJbXRtVXpKc1JuUTFVM0ZWVDJRM2RtNXBkRXhLY2pWTmRVcE9RMkpXVDBrM1p5c3JhV1JqYzFGWE5sd3ZRMVJrZW5oWldGcFFkVGgwWXpoNlNWSkpVa0pJY0dReFhDOWlXV3BQUjB0RFpFaFlka3BMZUhkRk1Gd3ZUVFJsVEVoRWJVeEtVVFkxZEhsSVVEZFZWRE5rYkRadlpYbFBkakZqVEZsWFVURTNOMGxoSzNZclVWZFJXbVZUVHpkd2RIQXpkMlF6Wm0weFVWVm9XVTVFZUROMFIySnZkbWxwTnpoQ2VtNDNNbkEzYTI1WmNuQnpVRFJ2Ymt4Rk1teHRXRXRXYzF3dlQyOXZjbXQzVWt4QmNtWldNMFl4U0VGVGFsaHpPSHBpYzNsSmNsTTJWMkZ2WXpObVZXaE5iVFJCTkc5cGNXNUVjVUZNVkdrd1N6VktiVE5rYjA1a1pWTlZjRVJzVUhCSGJtMUZiSFJDVnpsQk5WaFdTVWREYlVkTFYzQjJYQzl2UWxoaVVFUnJjVlJ1SzBadGJDdDJOVTFGUjFWRk56bHhRMUF4TURkQ1UyNVNaVmRrTUhsMFUycGNMM0Z4WXpONVdFMTZaM2RaVTNreVFYTmNMMUpUUml0UmMyZFlabWw2VGtzclp6ZFhlbFppWldWMVl6Wm5ibm81U0VrMEswYzJSRmRwU0ZrMU5tZEdaakZEYUVSU1JEbFBlV00wVkZSdWFtVmxkalZpUzJwMmIyTkZNa0p4YmtkdlR6RkNTbWRWTlVKNVdrZGNMMVpFWTFodE9WbGNMMnhTT0RFeFNYQmllRVZMZDBSM1VWRlZWM0JjTDFkbmFpdG9ZMVZwU1VWRVNuaGhPRTVxVXpWR1JEZFpVR1ZWV25rMlluSXhNbmd4YmtWeFRHbDBYQzgxYm1wY0wwZElaM053Y2xSNFozRk5Ra0o1TkdKMGFXaHhlRVpTYVV4cUt6aHpZMnBhVkRsSFMxZDNlREE0WVVSeGMyRTBhMjFOVkhKeWNXcFpNRVl6VlZaUlJ6RjZSVWN4WmlJc0ltMWhZeUk2SW1JeVltUTNNak0xWldJeE4yVTVOamszWlRoaU5qWTJPV1JoWXpka09EWXhNVFZpTVRKaFpUaGtOakF6WmprNVlqTTBOak14TldNeE0ySmhZV0kwTXpnaWZRPT0=', 1528163059);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cart_session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `cart_session_id`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$cbLRtpjUTkQPZRFtAUHPc.60/OhHsTGJYMAz2lUohcqdvTTSg2Yi2', 1, 'e8acf4eb44ff6a91db153be865e66d70', 1, '1hL7UQGYsjJTbh9ihFtG3gFg4oR7mANtRr13QXiAm7PABAWdhaMqdjZdyUUD', '2017-04-18 01:44:00', '2018-06-03 10:43:51', NULL, NULL),
(2, 'fname lname', 'example@example.com', '$2y$10$AAqUDoJjUqJoHf28l83tFOejmUIRQbKSEghNr5prFgd0IFxvp3wde', 1, 'cdb6b9352c163d29668ca042f54e6937', 0, '8AcfxchuKtSARNv6zpOYxej3js0jcQXwfbEMyKarVKtkI74hSmw4lwOPWnQH', '2018-05-30 22:42:25', '2018-06-02 00:57:13', NULL, NULL),
(4, 'lakshya maharjan', 'lakshya.punkrock@gmail.com', '$2y$10$Z8pluXMypRrWI8E7Y5VW5eBleAQ713ElgXw8VqtxaQeYlmDjsBUFi', 1, '142862994a2635a0017e2225baadae10', 1, 'IWHFEeJHLYXytUybpLiuASEgfN7oLHSSzrvk0rALuTT4UOy5IVdtd1tkLTN1', '2018-05-31 10:10:28', '2018-06-03 00:22:44', NULL, NULL),
(5, 'yojan Shrestha', 'yojansh50@gmail.com', '$2y$10$kJLtzNi5oKPx02s0lmj..OOnzSqGiugmb.zFgs/tPVkeF/J1f4vFu', 1, 'e651f5f698b8320dfd2339eb591a34e2', 1, 'zSbAUnZ0Af5csVkQ9YhZjd4AT77TQ1EsU6fcKfubXgzS9HKKzwOu5RiupUju', '2018-06-04 11:23:21', '2018-06-04 11:45:56', NULL, NULL),
(7, 'kjd asdf', 'kafj@aldf.com', '$2y$10$APkU8VdmdUX82Hw0hrsb9.U6Blwm8qntg0twwSd.1jLIyWnCws.HG', 1, '06bcb20562f02221c03a221002d2d19a', 0, NULL, '2018-06-04 17:54:53', '2018-06-04 17:54:53', NULL, NULL),
(8, 'fname lname', 'example2@example.com', '$2y$10$fG97PA8gZWs5iFGpRhtNFusbbKtauFbkqgnrlq5FyzqJp0nXS66fW', 1, '450431bba9e7673e21a50aea1ad120bf', 0, NULL, '2018-06-04 17:58:48', '2018-06-04 19:00:18', NULL, NULL),
(9, 'test5', 'test5@gmail.com', '$2y$10$7sCkTk0y2r6FC35Z/LxtWOTYIa9oTcN2cWSMumrj8Qbu3G2EWDrwK', 1, 'fee164339bc2880136a03f083fd8971b', 0, NULL, '2018-06-04 19:02:09', '2018-06-04 19:02:25', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_cancelled`
--
ALTER TABLE `order_cancelled`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `order_cancelled`
--
ALTER TABLE `order_cancelled`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_cancelled`
--
ALTER TABLE `order_cancelled`
  ADD CONSTRAINT `order_cancelled_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
