$(document).ready(function(){

$('#buyActionForm').validate()

	$(document).on('click', 'label.attribute', function(){

		$(this).closest('.singleAttribute').find('.attrName').val($(this).prev().data('name')); 
		$(this).closest('.singleAttribute').find('.attrValue').val($(this).prev().data('value')); 

		setTimeout(function() 
		{
			var ajaxCall = 0;
			if($('.singleAttribute').length == $('.attrVal:checked').length) 
			{
				var checkedVal = $('.attrVal:checked').map(function(){
					return $(this).val();
				}).get();
				ajaxCall = 1 ;
			}
			if(ajaxCall == 1)
			{ 
				NProgress.start();
				$.ajax({
					type: 'POST',
					url: base_url + '/product/getquantity',
					headers: { 'X-CSRF-Token': $('input[name=_token]').val() },
					data: { productid: $('input[name=product]').val(), values: checkedVal },
					success:function(response) 
					{
						if(response.state == 'success'){
							var manageStock = response.values[0].manage_stock;
							var availability = response.values[0].availability;
							var identifier = response.values[0].identifier;
							var quantity = response.values[0].quantity;
							if(manageStock == 1){
								if(availability == 'in stock'){
									if(quantity != 0){
										$('.remainQuantity span').text( quantity + ' is available');
										$('.quantity').attr('max',quantity);
										$('.attrIdentifier').val(identifier);
										$('.quantity').val($('.quantity').attr('min'));
										$('.QtyValidation').addClass('d-none');
										$('.btn-minus, .btn-plus').removeClass('disabled');
										$('.product-btns').removeClass('disabled');
										NProgress.done();
									} else {
										$('.remainingQuantity span').text('Product combination is not available.');
										$('.quantity').val($('.quantity').attr('min'));
	                                    $('.attrIdentifier').val('');
										$('.QtyValidation').addClass('d-none');
										$('.btn-minus, .btn-plus').addClass('disabled');
										$('.product-btns').addClass('disabled');
										NProgress.done();
									}
								} else {
									$('.attrIdentifier').val('');
									$('.btn-minus, .btn-plus').addClass('disabled');
									$('.product-btns').addClass('disabled');
									NProgress.done();

								}
							} else {
								if(quantity == 0){
									$('.quantity').attr('max',99999999);
									$('.QtyValidation').addClass('d-none');
	                                $('.attrIdentifier').val(identifier);
									$('.btn-minus, .btn-plus').removeClass('disabled');
									$('.product-btns').removeClass('disabled');
									NProgress.done();
								} else {
									$('.remainQuantity span').text(quantity + ' is available');
									$('.quantity').attr('max',quantity);
	                                $('.attrIdentifier').val(identifier);
									$('.QtyValidation').addClass('d-none');
									$('.btn-minus, .btn-plus').removeClass('disabled');
									$('.product-btns').removeClass('disabled');
									NProgress.done();
								}
							}
						} else {
							$('.remainingQuantity span').text('Product combination is not available.');
							$('.quantity').val($('.quantity').attr('min'));
	                        $('.attrIdentifier').val('');
							$('.QtyValidation').addClass('d-none');
							$('.btn-minus, .btn-plus').addClass('disabled');
							$('.product-btns').addClass('disabled');
							NProgress.done();
						}
						// console.log(response);
					},
					error:function(response)
					{
						$('.remainingQuantity span').text('Error, please try in a while.');
	                    $('.attrIdentifier').val('');
						$('.btn-minus, .btn-plus').addClass('disabled');
						$('.QtyValidation').addClass('d-none');
						$('.product-btns').addClass('disabled');
						NProgress.done();

					},
				});
			}
		}, 2000);

	});

	$('#add-to-cart').on('click', function(){
		$('#productActionForm input[name="action"]').val('addToCart');
		$('#productActionForm').submit();
	});

	$('#buy-now').on('click', function(){
		$('#productActionForm input[name="action"]').val('buynow');
		$('#productActionForm').submit();
	});

// $('#buy-now').on('click', function(){
// 	if ($("input[name='attr[0]']").is(":checked"))
// 	{
// 	  	var val = $("input[name='attr[0]']").val();
// 	}
// 	if ($("input[name='attr[1]']").is(":checked"))
// 	{
// 	  	var val2 = $("input[name='attr[1]']").val();
// 	}
//   	var qty = $(".quantity").val();
//   	var slug = $(".proslug").val();
//   	var proid = $(".proid").val();

//   	$.ajax({
//    	 				type: 'Get',
// 					url: base_url + '/buynow/'+slug,
// 					headers: { 'X-CSRF-Token': $('input[name=_token]').val() },
// 					data: { 
// 						qty: qty ,
// 						val: val ,
// 						val2: val2,
// 						id:proid 
// 					},
// 					dataType: 'JSON',
// 					success:function(response) 
// 					{
// 					},
// 					error:function(values){

// 	}
//    	 });

// 	});

   $(document).on('click','#addcart', function(){
   	 var prodid = $(this).attr('data-id');
   	 $('body').addClass('loadershow')
   	 $('.loader').show()
   	 $.ajax({
   	 				type: 'POST',
					url: base_url + '/addCart',
					headers: { 'X-CSRF-Token': $('input[name=_token]').val() },
					data: { productid: prodid },
					dataType: 'JSON',
					success:function(response) 
					{
   	 					$('.loader').hide()
   	 					$('html, body').animate({scrollTop: '0px'}, 0);
   	 					$('#exampleModalCenter').addClass('show')
   	 					$('#exampleModalCenter').css({'display':'block'})
   	 					$('#exampleModalCenter').attr('aria-hidden','')
   	 					$('body').removeClass('loadershow')
						if(response.val == 'true'){
							var num = $('.header-tools span.cart-count').html()
							var cart = parseInt(num) + parseInt(1);
							$('.header-tools span.cart-count').html(cart)
						}
					},
					error:function(values){

					}
   	 });
   })

   $('#exampleModalCenter .close').on('click',function(){
   	$('#exampleModalCenter').removeClass('show')
   	$('#exampleModalCenter').css({'display':'none'})
   	$('#exampleModalCenter').attr('aria-hidden','true')
   })


    $('.similar-products .owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    //items:4,     
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,  
    		navs: true,
        },
        600:{
            items:3,
            dots: true,  
    		navs: true,
        },
        1000:{
            items:4,
            dots: true,  
    		navs: true,
        }
    } 
    }) 

});
