
    function sliderCarousel() {
    	$('.slider .owl-carousel').owlCarousel({
    		items:1,
    		dots:true,
             responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    dots: true,  
                    navs: true,
                },
                600:{
                    items:1,
                    dots: true,  
                    navs: true,
                },
                1000:{
                    items:1,
                    dots: true,  
                    navs: true,
                }
            } 
    	})
    }

    function number_format_jquery( number, decimals, dec_point, thousands_sep ) {
        // http://kevin.vanzonneveld.net
        // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +     bugfix by: Michael White (http://crestidg.com)
        // +     bugfix by: Benjamin Lupton
        // +     bugfix by: Allan Jensen (http://www.winternet.no)
        // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)    
        // *     example 1: number_format(1234.5678, 2, '.', '');
        // *     returns 1: 1234.57     
     
        var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
        var d = dec_point == undefined ? "," : dec_point;
        var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
        var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
     
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }

    $(document).ready(function() {
        var sumAmt  = 0;
        $('.cart-review .sub-total span.product-total').each(function(){
            sumAmt += parseFloat($(this).data('amt'));
        });
        $('.totals-cart span.cart-sub-total em').html(number_format_jquery(sumAmt,2,'.',',').replace(".00",""));
        $('.totals-cart span.cart-grand-total em').html(number_format_jquery(sumAmt,2,'.',',').replace(".00",""));
        $('input[name=subTotal], input[name=grandTotal]').val(sumAmt.toFixed(2));
    });

    //check if both attribute is selected or not
    function checkAttribute() {
        if ($('.singleAttribute').length != $('.attrVal:checked').length ) {
            return false;
        }
            return true;
    }
    // ajax call here
    function qtyUpdate(elem, hash, newQty) { 
        NProgress.start();
        $.ajax({
            type    : 'POST',
            url     : base_url + '/cart/updateqty/' + hash,
            headers : { 'X-CSRF-Token' : $('input[name=_token]').val() },
            data    : { value : newQty },
            success : function(response) {
                if(response.state == 'success') {
                    var cartReviewContent   = elem.closest('.cart-review-content');
                    var price               = cartReviewContent.find('.cart-price span em').html();
                    var subtotal            = (price * newQty);
                    var sumQty              = 0;
                    var sumAmt              = 0;
                    var str                 = number_format_jquery(subtotal,2,'.',',');
                    
                    cartReviewContent.find('.sub-total span.product-total em').html(str.replace(".00",""));
                    cartReviewContent.find('.sub-total span.product-total').data('amt', subtotal.toFixed(2));
                    elem.val(newQty);
                    $('.cart-review input[name=qty]').each(function(){
                        sumQty += parseInt($(this).val());
                    });
                    $('.shopping-cart .cart-confirm span.count-cart').html('('+sumQty+' items)');
                    $('.header-tools li span.cart-count').html(sumQty);
                    
                    $('.sub-total span.product-total').each(function(){
                        sumAmt += parseFloat($(this).data('amt'));
                    });
                    $('.totals-cart span.cart-sub-total em').html(number_format_jquery(sumAmt,2,'.',',').replace(".00",""));
                    $('.totals-cart span.cart-grand-total em').html(number_format_jquery(sumAmt,2,'.',',').replace(".00",""));
                    cartReviewContent.find('.message').html(response.message);
                    NProgress.done();
                } else {
                    elem.val(newQty);
                    cartReviewContent.find('.message').html(response.message);
                    NProgress.done();
                }
            },
            error   : function(response) {
                cartReviewContent.find('.message').html('Something went wrong.Please try again later.');
                NProgress.done();
            }   
        });
    }
    // ajax call end

    function cart() {
        //-- Click on QUANTITY (Bydeveloper)
        $(".btn-minus").on("click",function() {
            if (! checkAttribute()) {
                $(this).closest('.qty-section').find('.QtyValidation span').text('Please select the product combination first');
                return false;
            }
            var elem    = $(this).next();
            var now     = elem.val();
            var min     = elem.attr('min');
            var max     = elem.attr('max');
            var hash    = elem.closest('.updateform').data('hash');
            var newQty  = '1';
            
            $('.message').html('');
                
            if ($.isNumeric(now)) {
                if (parseInt(now) > min)
                    { 
                        now--;
                        elem.closest('.qty-section').find('.remainingQuantity').removeClass('d-none');
                        elem.closest('.qty-section').find('.QtyValidation').addClass('d-none');
                        elem.closest('.qty-section').find('.remainQuantity span').text(max+' is available.');
                        elem.closest('.qty-section').find('.remainingQuantity span, .QtyValidation span').text('');
                    } else {
                        elem.closest('.qty-section').find('.remainingQuantity').addClass('d-none');
                        elem.closest('.qty-section').find('.QtyValidation').removeClass('d-none');
                        elem.closest('.qty-section').find('.QtyValidation span').text('Minimum quantity selected.');
                        elem.closest('.qty-section').find('.remainingQuantity span, .remainQuantity span').text('');
                    }
                newQty = now;
            } 
            if ($('body').hasClass('cart-page')) {
                qtyUpdate(elem, hash, newQty);
            } else {
                elem.val(newQty);
            }
        });            

        $(".btn-plus").on("click",function() {

            if (! checkAttribute()) {

                $(this).closest('.qty-section').find('.QtyValidation span').text('Please select the product combination first');
                return false;
            }
            var elem    = $(this).prev();
            console.log(elem)
            var now     = elem.val();
            var min     = elem.attr('min');
            var max     = elem.attr('max');
            var hash    = elem.closest('.updateform').data('hash');
            var newQty  = '1';
            $('.message').html('');
            if ($.isNumeric(now)) {
                // alert(min)
                // alert(max)
                // alert(now)
                if(parseInt(now) < max){
            // alert('now less thabn max')

                    now++;
                    elem.closest('.qty-section').find('.remainingQuantity').removeClass('d-none');
                    elem.closest('.qty-section').find('.QtyValidation').addClass('d-none');
                    elem.closest('.qty-section').find('.remainQuantity span').text(max+' is available.');
                } else {
            // alert('now more thabn max')

                    elem.closest('.qty-section').find('.remainingQuantity').addClass('d-none');
                    elem.closest('.qty-section').find('.QtyValidation').removeClass('d-none');
                    elem.closest('.qty-section').find('.QtyValidation span').text('Maximum quantity selected.');
                    elem.closest('.qty-section').find('.remainingQuantity span, .remainQuantity span').text('');
                }
                newQty = now;
            }
            if ($('body').hasClass('cart-page')) {
                qtyUpdate(elem, hash, newQty);
            } else {
                elem.val(newQty);
            }
        });       
    }

    $('.submitConfirm').on('click', function() {
        var elem    = $(this).closest('.cart-review-content');
        elem.find('.removeCartItem').submit();
    }); 

    $('.cart-to-checkout button').on('click', function() {
        $('.checkout-form').submit();
    });     

    $('div.buy-now input[name=qty]').keyup(function(){
        if (! checkAttribute()) {
            $(this).closest('.qty-section').find('.QtyValidation span').text('Please select the product combination first');
            return false;
        } 
        var elem        = $(this).closest('.qty-section');
        var qtyValue    = $(this).val();
        var min         = $(this).attr('min');
        var max         = $(this).attr('max');
        if ($.isNumeric(qtyValue)) {
            if (parseInt(qtyValue) > max) {
                elem.find('.remainingQuantity').addClass('d-none');
                elem.find('.QtyValidation').removeClass('d-none');
                elem.find('.QtyValidation span').text('Maximum quantity crossed.');
                elem.find('.remainQuantity span, .remainingQuantity span').text('');
            } else {
                elem.find('.remainingQuantity').removeClass('d-none');
                elem.find('.QtyValidation').addClass('d-none');
                elem.find('.QtyValidation span').text('Minimum quantity crossed.');
                elem.find('.remainQuantity span, .remainingQuantity span').text('');

            }
        } else {
            elem.find('.remainingQuantity').addClass('d-none');
            elem.find('.QtyValidation').removeClass('d-none');
            elem.find('.QtyValidation span').text('Invalid charaters.Please enter numbers.');
            elem.find('.remainQuantity span, .remainingQuantity span').text('');
        }
    });

    $(document).ready(function() {
    	sliderCarousel();
    	$(".xzoom").xzoom();
    	cart();
    });

    $(window).resize(function() {
    	sliderCarousel();
    });

    $('button.place-order').on('click', function() {
        $('.checkout-form div.checkout-section').toggle();
    });