$(document).ready(function(){
	$('.profile-form').validate()
	$('input[type="checkbox"]').click(function() {
	$('input[type="checkbox"]').each(function() {		
        if ($(this).is(":checked")) {
		$(this).parent().parent().parent().find('.qty').removeAttr('disabled')
		$(this).parent().parent().parent().find('.totalprice').removeAttr('disabled')
		$('.btnsubmit').removeAttr('disabled')
       }else{
		$(this).parent().parent().parent().find('.qty').attr('disabled','disabled')       	
		$(this).parent().parent().parent().find('.totalprice').attr('disabled','disabled')       	
       }
    });
    });

$('.qty').on('keyup mouseup',function(){
	var price = $(this).parent().parent().find('span.price').html();
	var qty = $(this).val()
	var total = (parseFloat(price) * qty);
	var totalval = total.toFixed(2);
	$(this).parent().parent().find('.totalprice').html(totalval);
	$(this).parent().parent().find('.totalprice').val(totalval);
})

 $('#product-tables').DataTable();

});