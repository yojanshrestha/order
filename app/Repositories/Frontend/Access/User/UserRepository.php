<?php

namespace App\Repositories\Frontend\Access\User;

use App\Events\Frontend\Auth\UserConfirmed;
use App\Exceptions\GeneralException;
use App\Models\Access\Role\Role;
use App\Models\Access\User\SocialLogin;
use App\Models\Access\User\User;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Input;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * @param $email
     *
     * @return bool
     */
    public function findByEmail($email)
    {
        return $this->query()->where('email', $email)->first();
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function findByToken($token)
    {
        return $this->query()->where('confirmation_code', $token)->first();
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function getEmailForPasswordToken($token)
    {
        $rows = DB::table(config('auth.passwords.users.table'))->get();

        foreach ($rows as $row) {
            if (password_verify($token, $row->token)) {
                return $row->email;
            }
        }

        throw new GeneralException(trans('auth.unknown'));
    }

    /**
     * @param array $data
     * @param bool  $provider
     *
     * @return static
     */
    public function create(array $data, $provider = false)
    {
        $user = self::MODEL;
        $user = new $user();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->status = 1;
        $user->password = $provider ? null : bcrypt($data['password']);
        $user->confirmed = $provider ? 1 : (config('access.users.confirm_email') ? 0 : 1);

        DB::transaction(function () use ($user, $data) {
            if ($user->save()) {
                //create profile of the user
                $user->profile()->create([
                    'apt'=>$data['apt'],
                    'city'=>$data['city'],
                    'phone'=>$data['phone'],
                    'address'=>$data['address'],
                    'state'=>$data['state'],
                    'fax'=>$data['fax'],
                    'company'=>$data['company'],
                    'website'=>$data['website'],
                    'taxid'=>$data['eni'],
                ]);

                /*
                 * Add the default site role to the new user
                 */ 
                 $user->attachRole($this->role->getDefaultUserRole());
                // if ($data['user_type'] == 'Customer' || $data['user_type'] == 'Vendor' || $data['user_type'] == 'WholeSeller') {
                //     $role = Role::where('name', $data['user_type'])->first();
                //     $user->attachRole($role);
                // }
            }
        });

        /*
         * If users have to confirm their email and this is not a social account,
         * send the confirmation email
         *
         * If this is a social account they are confirmed through the social provider by default
         */
        if (config('access.users.confirm_email') && $provider === false) {
            Mail::send('emails.activateemail',['user'=>$user,'data'=>$data], function($message) use($user){
                $message->to($user['email'], $user['name'])
                ->subject('User registration.');
            });

            Mail::send('emails.useremail',['user'=>$user,'data'=>$data], function($message) use($user){
                $message->to('info@royaldogchew.pet')
                ->subject('User registration.');
            });
            // $user->notify(new UserNeedsConfirmation($user->confirmation_code,$data ));
        }

        /*
         * Return the user object
         */
        return $user;
    }

    /**
     * @param $data
     * @param $provider
     *
     * @return UserRepository|bool
     * @throws GeneralException
     */
    public function findOrCreateSocial($data, $provider)
    {
        // User email may not provided.
        $user_email = $data->email ?: "{$data->id}@{$provider}.com";

        // Check to see if there is a user with this email first.
        $user = $this->findByEmail($user_email);

        /*
         * If the user does not exist create them
         * The true flag indicate that it is a social account
         * Which triggers the script to use some default values in the create method
         */
        if (! $user) {
            // Registration is not enabled
            if (! config('access.users.registration')) {
                throw new GeneralException(trans('exceptions.frontend.auth.registration_disabled'));
            }

            $user = $this->create([
                'name'  => $data->name,
                'email' => $user_email,
            ], true);
        }

        // See if the user has logged in with this social account before
        if (! $user->hasProvider($provider)) {
            // Gather the provider data for saving and associate it with the user
            $user->providers()->save(new SocialLogin([
                'provider'    => $provider,
                'provider_id' => $data->id,
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]));
        } else {
            // Update the users information, token and avatar can be updated.
            $user->providers()->update([
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]);
        }

        // Return the user object
        return $user;
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function confirmAccount($token)
    {
        $user = $this->findByToken($token);

        if ($user->confirmed == 1) {
            throw new GeneralException(trans('exceptions.frontend.auth.confirmation.already_confirmed'));
        }

        if ($user->confirmation_code == $token) {
            $user->confirmed = 1;

            event(new UserConfirmed($user));

            return $user->save();
        }

        throw new GeneralException(trans('exceptions.frontend.auth.confirmation.mismatch'));
    }

    /**
     * @param $id
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function updateProfile($id, $input, $request='')
    {

        $user = $this->find($id);

        $user->name = $input['username'];
     
        DB::table('profiles')
                            ->where('user_id', $id)
                            ->update([
                                'apt'=>$input['apt'],
                    'city'=>$input['city'],
                    'phone'=>$input['phone'],
                    'address'=>$input['address'],
                    'state'=>$input['state'],
                    'fax'=>$input['fax'],
                    'company'=>$input['company'],
                    'website'=>$input['website'],
                    'taxid'=>$input['eni'],
                              ]);  
            return $user->save();


    }

    public function updateUser($id, $input)
    {
        
    }

    /**
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function changePassword($input)
    {
        $user = $this->find(access()->id());

        if (Hash::check($input['old_password'], $user->password) && $input['password'] == $input['password_confirmation']) {
            $user->password = bcrypt($input['password']);

            return $user->save();
        }
        if($input['password'] != $input['password_confirmation']){
        throw new GeneralException(trans('Password is mismatched'));
        }
        throw new GeneralException(trans('exceptions.frontend.auth.password.change_mismatch'));
    }
}
