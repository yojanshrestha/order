<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class FrontendController extends Controller
{

    public function index()
    {
        return redirect()->route('admin.dashboard');
        return "homepage";
    }
    
    public function userDashboard(){
        return redirect()->route('admin.dashboard');
        return "dashboard";
    }

}
