<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Access\User\User;
use App\Product\Order;
use App\Product\OrderProduct;
use Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($status = $request->status)) {
        	$orders 	= Order::select('id','identifier','user_id','total','status')->where('status', $status)->get();
        }else{
            $orders     = Order::select('id','identifier','user_id','total','status')->get();
        }
    	
        return view('backend.orders.index', compact('orders'));
    }
    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $vieworder = Order::findOrFail($id);

        return view('backend.orders.view',compact('vieworder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($status, $id)
    {
        $order      = Order::where('id', $id)->first();
        if ($status == 'verified' && $order->status == 'pending') {
            $order->status = $status;
            $order->save();

            return redirect()->route('admin.orders')->withFlashSuccess('Order has been verified.');
        } elseif ($status == 'completed' && $order->status == 'verified') {
            $check      = 1;
            foreach ($order->orderItems as $key => $orderItem ) {
                $productqty     = $orderItem->product->qty;
                $orderqty       = $orderItem->qty;
                if ($productqty < $orderqty) {
                    $check = 0;
                    break;
                }
            }
            if ($check == 1) {
                $order->status = $status;
                $order->save();
                if ($order->status == 'completed') {
                    foreach ($order->orderItems as $key => $orderItem ) {
                        $orderItem->product->qty -= $orderItem->qty;
                        if ($orderItem->product->qty == 0) {
                            $orderItem->product->inventory = 'out of stock';
                        }
                        $orderItem->product->save();
                    } 
                }
                return redirect()->route('admin.orders')->withFlashSuccess('Order has been completed.');
            } else {
                return redirect()->route('admin.orders')->withFlashWarning('Order has not been completed.Please verify product quantity.');
            }
        } elseif ($status == 'cancelled' && ($order->status == 'pending' || $order->status == 'verified') && $order->status != 'completed') {
            $order->status          = $status;
            $order->order_cancel    = 1;
            $order->cancel_date     = Carbon::now();
            $order->save();
        }
            return redirect()->route('admin.orders')->withFlashSuccess('Order has been cancelled.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
