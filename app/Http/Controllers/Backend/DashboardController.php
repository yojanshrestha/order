<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Product\Order;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $activeUsers = User::active()->confirmed()->count();
        $inActiveUsers = User::active()->confirmed(false)->count();
        $pendingOrders = Order::where('status', 'pending')->count();
        $verifiedOrders = Order::where('status', 'verified')->count();
        $completedOrders = Order::where('status', 'completed')->count();
        $cancelledOrders = Order::where('status', 'cancelled')->count();
        return view('backend.dashboard', compact('activeUsers', 'inActiveUsers', 'pendingOrders', 
            'verifiedOrders', 'completedOrders', 'cancelledOrders'));
    }

     public function profile()
    {
        return view('user.profile',compact(''))->withClass('profile-info');
    }

    public function passwordchange()
    {
        return view('user.password')->withClass('password-info');
    }

}
