<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Product\Order;
use App\Product\OrderProduct;
use App\Product\CancelOrder;
use Auth;
use Mail;
use Carbon\Carbon;


class OrderListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orderlist = Order::where('user_id', Auth::user()->id)->where('order_cancel',0)->get();
        return view('user.product.orderlist', compact('orderlist'));
    }
    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $orderview = Order::where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (empty($orderview)) {
            abort(404);
        }
        return view('user.product.orderview',compact('orderview'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order = Order::findOrFail($id);
        if ($order->status != 'pending') {
            abort(404);
        }
        $order->update([
            'order_cancel'  => 1,
            'status'        => 'cancelled',
            'cancel_date'   => Carbon::now(),
            ]);
        
        if($cancel){
            Mail::send('emails.ordercancel',['order'=>$order], function($message) use($order){
                $message->to('info@royaldogchew.pet')
                ->subject('Order Cancel');
        });
        }
        return redirect()->route('admin.orderlist.index')->withFlashSuccess('Order has been cancelled');

    }
}
