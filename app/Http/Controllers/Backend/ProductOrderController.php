<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Product\Order;
use App\Product\OrderProduct;
use App\Models\Access\User\Profile;
use Auth;
use Mail;

class ProductOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::where('status',1)->where('inventory','in stock')->get();
        return view('user.product.list',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        //
        $sum = 0;
        foreach($request->totalprice as $key => $tp)
         {
            $sum += $tp; 
         }   
      $user = Profile::where('user_id',Auth::user()->id)->first();
        $order = Order::create([
            'identifier' => str_random(),
            'user_id' => Auth::user()->id,
            'sub_total' => $sum,
            'total' => $sum,
            'status' =>'pending',
            'order_cancel' => '0',
            ]);
        if($order){
        foreach($request->bulk_select as $key => $select){
            $singleprice = $request->totalprice[$key] / $request->qty[$key];
            OrderProduct::create([
                    'order_id' =>$order->id,
                    'product_id' => $request->bulk_select[$key],
                    'qty' => $request->qty[$key],
                    'rate' => $singleprice,
                ]);
        }
        Mail::send('emails.ordersend',['order' => $order, 'user' => $user, 'name' => Auth::user()->name], function($message) use($order){
                $message->to('info@royaldogchew.pet')
                ->subject('Order Placed');
        });
        }

        return redirect()->route('admin.orderlist.index')->withFlashSuccess('Order has been created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
