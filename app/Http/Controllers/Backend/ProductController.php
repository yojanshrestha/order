<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Product\CreateProductRequest;
use App\Http\Requests\Backend\Product\DeleteProductRequest;
use App\Http\Requests\Backend\Product\UpdateProductRequest;
use App\Models\Access\User\User;
use App\Models\Product\Product;
use DB;
use Datatables;
use File;
use Illuminate\Http\Request;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select('id','feat_img','name','slug','created_at','updated_at','price','status')->get();
            return view('backend.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  App\Http\Requests\Backend\Product\CreateProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateProductRequest $request)
    {
        return view('backend.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Backend\Product\CreateProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $this->validate($request,[
            'name'          => 'required',            
            'status'        => 'required',  
            'slug'          => 'required',    
            'price'         => 'required|numeric',
            'qty'           => 'required|numeric',
            'inventory'     => 'required',
        ]);

        if ($request->hasFile('feat_img')) {
            $temp_file      = $request->file('feat_img');
            $destination    = 'images/products/';
            $filename       = str_random(5).'-'.$temp_file->getClientOriginalName();
            $move           = $temp_file->move($destination, $filename);
        } else {
            $filename       = '';
        }
        $product = Product::create([
            'user_id'       => Auth::user()->id,
            'name'          => $request->name,
            'slug'          => $request->slug,
            'url'           => $request->url,
            'price'         => $request->price,
            'qty'           => $request->qty,
            'inventory'     => $request->inventory,
            'status'        => $request->status,
            'description'   => $request->description,
            'feat_img'      => $filename,
        ]);
        return redirect('user/products')->withFlashSuccess('Product information stored successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Http\Requests\Backend\Product\UpdateProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, UpdateProductRequest $request)
    {
        $products = Product::findOrFail($id);

        return view('backend.products.edit', compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Backend\Product\UpdateProductRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateProductRequest $request)
    {
      $this->validate($request,[
            'name'          => 'required',            
            'status'        => 'required',  
            'slug'          => 'required',    
            'price'         => 'required|numeric',
            'qty'           => 'required|numeric',
            'inventory'     => 'required',
        ]);

        $product = Product::findOrFail($id);

        if ($request->hasFile('feat_img')) {
            $temp_file      = $request->file('feat_img');
            $destination    = 'images/products/';
            $filename       = str_random(5).'-'.$temp_file->getClientOriginalName();
            $move           = $temp_file->move($destination, $filename);
            if ($move) {
                if (!empty($product->feat_img)) {
                    if (File::exists('images/products/'.$product->feat_img)) {
                        unlink('images/products/'.$product->feat_img);
                    }
                }
            }
        } else {
            $filename       = $product->feat_img;
        }
        $product->update([
            'user_id'       => Auth::user()->id,
            'name'          => $request->name,
            'slug'          => $request->slug,
            'url'           => $request->url,
            'price'         => $request->price,
            'qty'           => $request->qty,
            'inventory'     => $request->inventory,
            'status'        => $request->status,
            'description'   => $request->description,
            'feat_img'      => $filename,
        ]);

        return redirect('user/products')->withFlashSuccess('Product information updated successfully.');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  App\Http\Requests\Backend\Product\DeleteProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, DeleteProductRequest $request)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        // delete_files($this->productDir.$id.'/');
        return redirect('/user/products')->withFlashSuccess('Product deleted successfully.');
    }
}
