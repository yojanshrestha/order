<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use DB;
use Illuminate\Support\Facades\Input;
/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
   * @param ChangePasswordRequest $request
   *
   * @return mixed
   */
  public function formPassword()
  {
     // $this->user->changePassword($request->all());

      return view('user.password')->withClass('password-change');
  }

  public function changePassword(Request $request)
  {
    dd($request->all());
    // return "hjhjh";
      $this->user->changePassword($request->all());

      return redirect()->route('admin.password')->withFlashSuccess(trans('strings.frontend.user.password_updated'));
  }


    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request)
    {

        
        $output = $this->user->updateProfile(access()->id(), $request->all());

        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            access()->logout();

            return redirect()->route('admin.login')->withFlashInfo(trans('strings.frontend.user.email_changed_notice'));
        }

        return redirect()->route('admin.profile')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    public function userUpdate(UpdateProfileRequest $request)
    {
        $output = $this->user->updateProfile(access()->id(), $request->all(), $request);
        return redirect('/user/user/profile')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));

    }

    // public function passwordUpdate(UpdateProfileRequest $request)
    // {
    //     $output = $this->user->updateProfile(access()->id(), $request->all());
    //     return redirect('/user/password')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));

    // }

    
}
