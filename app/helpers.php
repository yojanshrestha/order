<?php

use App\Models\Cartitem\Cartitem;
use App\Models\GeneralSetting;
use App\Models\Product\Product;
use App\Models\Category;
use App\Models\ProductGroup\ProductGroup;
use App\Models\Access\User\User;
use App\Models\Brand;
use App\Models\Member;
use App\Models\Ads;


/**
 * Global helpers file with misc functions.
 */
if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('app_developer')) {
    /**
     * Helper to grab the developer agency name. --yojan
     *
     * @return mixed
     */
    function app_developer()
    {
        return config('app.developer');
    }
}

if (! function_exists('access')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function access()
    {
        return app('access');
    }
}

if (! function_exists('history')) {
    /**
     * Access the history facade anywhere.
     */
    function history()
    {
        return app('history');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('includeRouteFiles')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function includeRouteFiles($folder)
    {
        $directory = $folder;
        $handle = opendir($directory);
        $directory_list = [$directory];

        while (false !== ($filename = readdir($handle))) {
            if ($filename != '.' && $filename != '..' && is_dir($directory.$filename)) {
                array_push($directory_list, $directory.$filename.'/');
            }
        }

        foreach ($directory_list as $directory) {
            foreach (glob($directory.'*.php') as $filename) {
                require $filename;
            }
        }
    }
}

if (! function_exists('getRtlCss')) {

    /**
     * The path being passed is generated by Laravel Mix manifest file
     * The webpack plugin takes the css filenames and appends rtl before the .css extension
     * So we take the original and place that in and send back the path.
     *
     * @param $path
     *
     * @return string
     */
    function getRtlCss($path)
    {
        $path = explode('/', $path);
        $filename = end($path);
        array_pop($path);
        $filename = rtrim($filename, '.css');

        return implode('/', $path).'/'.$filename.'.rtl.css';
    }

if (! function_exists('parseDateTimeY_M_D')) {
    /**
     * this is method can be used to get date in 2017/04/23 format
     */
    function parseDateTimeY_M_D($date)
    {
        return \Carbon\Carbon::parse($date)->toDateTimeString();
    }
}



if (! function_exists('crudOps')) {
    /**
     * this is method can be used to generate edit and delete button in table
     * @param string $resource eg:admin.pages.edit where resource = pages
     * @param $id 
     * @return $string
     */
    function crudOps($resource, $id, $del = null)
    {
        $ops = '<ul class="list-inline no-margin-bottom">';
        $ops .= '<li><a href="'.route('admin.'.$resource.'.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a></li>';
        if (!empty($del)) {
            if (!in_array($id, $del)) {
                $ops .= '<li><a href="'.route('admin.'.$resource.'.destroy', $id).'" data-method="delete" name="delete_item" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a></li>';
            }
        } else {
            $ops .= '<li><a href="'.route('admin.'.$resource.'.destroy', $id).'" data-method="delete" name="delete_item" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a></li>';
        }
        $ops .= '</ul>';
        return $ops;
    }
}

if(!function_exists('parseStatus')){

    function parseStatus($status)
    {
        if ($status==1) {
            return '<label class="label label-success">Enabled</label>';

        }else{

            return '<label class="label label-danger">Disabled</label>';
        }
    }
}



if (! function_exists('generateUniqueSlug')) {
    /**
     * this is method can be used to generate slug
     * @param string $classPath eg: App\Models\Page
     * @param $title 
     * @return $string
     */
    function generateUniqueSlug($classPath, $title)
    {
        $temp = str_slug($title, '-');
        if(!($classPath::where('slug',$temp)->get()->isEmpty()) ){
        $i = 1;
        $newslug = $temp . '-' . $i;
        while(!($classPath::where('slug',$newslug)->get()->isEmpty()) ){
            $i++;
            $newslug = $temp . '-' . $i;
        }
        $temp =  $newslug;
    }
    return $temp;
    }
}

if (! function_exists('deleteFile')) {
    /**
     * this is method can be used to delete file if exists
     * @param $string 
     */
    function deleteFile($filepath)
    {
        if(File::exists($filepath)){
            unlink($filepath);
        }
    }
}


}
