<?php

namespace App\Product;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';

    protected $guarded = ['id'];

    public function orderItems(){
    	 return $this->hasMany('App\Product\OrderProduct','order_id');
    }

    public function productName($id) 
    {
        $product = Product::findOrFail($id);
        return $product->name;
    }
    public function productImage($id) 
    {
    	$product = Product::findOrFail($id);
    	return $product->feat_img;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'user_id');
    }

    public function getOrderStatusAttribute()
    {
        if ($this->status == 'pending') {
            return "<label class='label label-warning'>".'Pending'.'</label>';
        } elseif ($this->status == 'verified') {
            return "<label class='label label-primary'>".'Verified'.'</label>';
        } elseif ($this->status == 'completed') {
            return "<label class='label label-success'>".'Completed'.'</label>';
        } elseif ($this->status == 'cancelled') {
            return "<label class='label label-danger'>".'Cancelled'.'</label>';
        }
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }

        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }
}
