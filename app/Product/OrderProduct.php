<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    protected $table = 'order_items';

    protected $guarded = ['id'];

    public function product()
    {
    	return $this->belongsTo('App\Models\Product\Product', 'product_id');
    }
}
