<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class CancelOrder extends Model
{
    //
    protected $table = 'order_cancelled';

    protected $guarded = ['id'];
}
