-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2018 at 04:15 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `order`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(17, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 09:36:46', '2018-05-31 09:36:46'),
(18, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 09:37:00', '2018-05-31 09:37:00'),
(19, 1, 1, 2, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 10:07:52', '2018-05-31 10:07:52'),
(20, 1, 1, 2, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","fname lname",2]}', '2018-05-31 10:08:05', '2018-05-31 10:08:05'),
(21, 1, 1, 4, 'times', 'bg-yellow', 'trans("history.backend.users.deactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-05-31 10:27:14', '2018-05-31 10:27:14'),
(22, 1, 1, 4, 'check', 'bg-green', 'trans("history.backend.users.reactivated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","lakshya maharjan",4]}', '2018-05-31 10:27:47', '2018-05-31 10:27:47');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2017-04-18 01:44:05', '2017-04-18 01:44:05'),
(2, 'Role', '2017-04-18 01:44:05', '2017-04-18 01:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2017-04-18 01:44:04', '2017-04-18 01:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `feat_img` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `inventory` enum('in stock','out of stock') NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `fname`, `lname`, `address`, `phone`, `gender`, `created_at`, `updated_at`) VALUES
(39, 2, 'fname', 'lname', NULL, '1234567890', NULL, '2018-05-30 22:42:25', '2018-05-30 22:42:25'),
(41, 4, 'lakshya', 'maharjan', 'badegau, lalitpur', '9841855515', 'male', '2018-05-31 10:10:28', '2018-05-31 10:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-04-18 01:44:01', '2017-04-18 01:44:01'),
(2, 'Wholesaler', 0, 4, '2017-06-16 06:11:18', '2017-10-30 02:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('5uwCmO7aqrj7kNmHC9DMiILDQ8ZJWcbCo1LAFkzl', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'ZXlKcGRpSTZJblEwVG0xQk1HZHBhMFJxWnpGaVYwUmxVVFZWVTFFOVBTSXNJblpoYkhWbElqb2lZVXBJYVZ3dk9GRk1TR3d6U0RkaFowMDFiek5HY1dwcFFtRlRhakY1VFZWTE5FNUJUVkJ5YTJWcWVrTnBTa1ZzZDAxMk9HWkRjMDAxU0VKRFkxd3ZjVWhKTVU0MlpqZHZWM0YyZEdwWUt6ZGhRV0pSZEdORlJXY3hZVlJ0VkdOeWJqTXdZVUZOWkdwT1JqbFlaak51WjFKR1VURlNaeXMyTjNSVlpuVXdVamRqWTNOdVRYSmlUblpWY0hKRGEybHlURWx4V1dSYVFXMTNhM0pTUXpsRmVVUlNOMUJoUldsV1VsbzFZVmRuTmxCTFVrdFlkbkpzYzJaRGF6SnBNVEJCUW1aMVlWQnBNM2N6Y1RnMGFXWktSRU16UkdFM00wTnhjbkJ6WjFJMVltbGlLMDl3TW5kTVhDOVZjazVXUVdkWVNFMXdkM2hKUWtvMlJGUTRUMGMxZW0xb1VFWnJTVm8wVkdaSFhDODBVbmh2WnpCeU5qaFdlSFIzT1RscFVETkJUSHBNV1RaUWVrNXdNRlJyWWx3dmR6ZFJValp2YkZWQ1RFbDBTVmhpUm5oVVJISXJkMVJjTDIxTFRHSllSbkp3VG5WaFNWbHpNWGRyU1VkMmRYRkVLM05QY1VadlNXaEtVWEJGYjBzNWRWd3ZRbXg1VFRkSU5sUnVRbVF3ZEhWSlUweGphbE5RWTBObVRHSlRNbkZZVDJnM1VFSjVkak5OTVROSU4xSlJNbFk0UzNKSFJsSkNSazgxYWxWS1ZITk5aMk41SzJvd1kxQkZZV0Z3YzFwV1IwWktVRVJwYXpoaFMzbElTVzFxZWtsSVRGSlRXV3M0YjBjclQyZFpPWGQzUlRseWREVllTM1pxVEVGMlVGTnFkREZSUFNJc0ltMWhZeUk2SW1RellqQXhORFJpTldGbE1UWXdOR0ZrWmpVeU16SXpNemd5TW1Nd1l6RTBNamszTTJFeVpqVXhZV0U0TVRVNVpHTXhNVGs1TXpCa09UTXlOR1JrT1dJaWZRPT0=', 1527942212),
('9cClk2jQe3Ycw1ZkgC6MhXugeHJyAWJHbEyDfryY', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'ZXlKcGRpSTZJbTVISzJSWVZsZHdla2x4UzBsME5qbDBPWFZzZDJjOVBTSXNJblpoYkhWbElqb2liV2NyU3poSmMwZEdjVE5MTkZkMk1ITXpSV2w0U2xScFNFbExUMHQwZW1aY0wwMUZka1o1Vm5Zd1JuRklZalJyVGs5dVhDOU9hblozYlRGdlprRTRaREJYWm5od1NrMXNjSGxZWmtzeE1sQnFhM0ZaVEZaVFdrdzVkVEJvZEdjeVZ6Z3pNWGxjTDB0RFkyWk9UMHhVTnpBd2IxcEhlbWMxT1U0d1hDOUxVa05JUnpoWk1WVnBaR2x1VGpGYVluaHRiWE0zTWxaYVZFVXhOVE5ZYUVzMlFVVm1ReXR0VFROc01VOTZkRFZjTDFOb2JXSk9SelYzT1hnMVZEQlNkRnBjTHpWQlJVZFZVblJaYkN0TmNrMWNMM2R0VUdacVVXRTBWV1F5VlhWbFJsQnhjRlpWU1VVMWJHbHlVVUl6TUhWM01XSTJWMGszTTJnck5FOHhSVnBLVkV4Q01HOXdXRTR3T1c1Q1QwNUZPSGhxTTB0V1JsZDRkVWcwZUdaNFdYTnNRVXB1YjFrd01ERjNjbFZGVEZOWVNXa3hjRzF4Y0ZkeFNYZFZSM1ZsY2xWSk1HWjZhRk5yVGxSUlJGYzVSbFZxYjNoNlFUbEVOMnhVYTBoeFFtMW9hM2xZZUdaeWRFeFVSMXd2TW5WVmVsZG9OWG8zWVhCNWVqQjBSWGhzZWsxQ1ZUUlpPV1pEYVU5c1NVSk5XRlowVEhSbldTdHdPWGxXTkUxbmNteFFNR2swZFVOdFNWTjRTRVZJT0hGNVoxaHZZbTh6T1VsSmRXWjJVMVIzUkhGUlNtNVhUblJDZURaVUlpd2liV0ZqSWpvaU1EUTNOekl3TkRFME5qSTRNekZsWWpaak5UUTVOakpoWlRNMlpqTmlNamt3TmpOaVpqTXlNV1F4TlRVNU16Sm1OMkpsTnpsbVlqa3lZakZsWmpGbE9DSjk=', 1527948643);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cart_session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `cart_session_id`) VALUES
(1, 'Three Hammers', 'admin@3hammers.com', '$2y$10$NEYa6a.UqW0Fqt.m5CjJ9e5zBf0C0NEVfyc2JiueEs4wYCAQAmQ4.', 1, 'e8acf4eb44ff6a91db153be865e66d70', 1, 'GEqhZr2cP4DVdSpht8Jvw75KUByOmmG78vuoXEzU568muKzwo1MKnErj8JTv', '2017-04-18 01:44:00', '2017-04-18 01:44:00', NULL, NULL),
(2, 'fname lname', 'example@example.com', '$2y$10$AAqUDoJjUqJoHf28l83tFOejmUIRQbKSEghNr5prFgd0IFxvp3wde', 1, 'cdb6b9352c163d29668ca042f54e6937', 0, '8AcfxchuKtSARNv6zpOYxej3js0jcQXwfbEMyKarVKtkI74hSmw4lwOPWnQH', '2018-05-30 22:42:25', '2018-05-31 10:08:05', NULL, NULL),
(4, 'lakshya maharjan', 'lakshya.punkrock@gmail.com', '$2y$10$Z8pluXMypRrWI8E7Y5VW5eBleAQ713ElgXw8VqtxaQeYlmDjsBUFi', 1, '142862994a2635a0017e2225baadae10', 1, 'jOWBN2Pv3SBzEOvnDJAmwRb25F2jjr867EkRPO6xBannH2UmXHltSeOOObOs', '2018-05-31 10:10:28', '2018-05-31 10:27:47', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
