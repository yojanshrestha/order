@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('Product List') }}</small>
    </h1>
@endsection

@section('content')

<?php 
    $user = Auth::user(); 
    $extrainfo = Auth::user()->profile; 
    $extra = Auth::user()->roles; 
    $info = DB::table('profiles')->where('user_id',$user->id)->first();
?>
    
    
    {{-- <div class="col-sm-8 col-md-8"> --}}
    


            <div class="col-sm-12 col-md-12 Fullcontent-wrap">
              <div class="userdash-right">
                <section class="profilehead-wrap">
                 
                  <div class="col-md-9">
                    <div class="phead-rtContent">
                      
                      <table class="info-table">
                        <tbody><tr class="info-proj proj-account" data-role="account">
                            <td class="info-proj-title">Your Member ID:</td>
                            <td class="info-proj-value" data-proj="value">@if(!empty($user->id)){{$user->id}}@endif</td>
                            <td class="info-proj-operate"></td>
                            <td class="info-proj-operate"></td>
                        </tr>
                        <tr class="info-proj proj-email" data-role="email">
                            <td class="info-proj-title">Your Email:</td>
                            <td class="info-proj-value" data-proj="value">@if(!empty($user->email)){{$user->email}}@endif</td>
                            <td class="info-proj-operate pdl20"><a href="{{ URL::to('user/user/password') }}" class="memberbtn_user btn btn-primary">Change Password</a></td>
                            <td></td>
                        </tr>
                        
                        </tbody>
                      </table>
                      
                    </div>
                  </div>
                </section>

                <section class="required_wrap">
                  {{ Form::open(['url' => 'user/profile/update/'.$user->id, 'method' => 'patch', 'class' => 'profile-form','files'=>'true']) }}
                  <div class="dashboardBorder">
                    <div class="reqpattern profilehead-wrap">
                       
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Username</label>
                          <div class="col-lg-9">
                            <input class="form-control required" name="username" value="@if(!empty($user->name)){{$user->name}}@endif" type="text">
                          </div>
                        </div>
                 
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Email address <i class="fa fa-lock"></i></label>
                          <div class="col-lg-9">
                            <input class="form-control email required" name="email" value="{{$user->email}}" type="text" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Contact address</label>
                            <div class="col-lg-9">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="address" value="@if(!empty($extrainfo->address)){{$extrainfo->address}}@endif" type="text">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">APT/STE</label>
                            <div class="col-lg-9">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="apt" value="@if(!empty($extrainfo->apt)){{$extrainfo->apt}}@endif" type="text">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">City</label>
                            <div class="col-lg-9">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="city" value="@if(!empty($extrainfo->city)){{$extrainfo->city}}@endif" type="text">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">State</label>
                            <div class="col-lg-9">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="state" value="@if(!empty($extrainfo->state)){{$extrainfo->state}}@endif" type="text">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Phone</label>
                          <div class="col-lg-9">
                            <input class="form-control phone required" name="phone" value="@if(!empty($extrainfo->phone)){{$extrainfo->phone}}@endif" type="text">
                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-lg-3 control-label">Fax</label>
                          <div class="col-lg-9">
                            <input class="form-control fax" name="fax" value="@if(!empty($extrainfo->fax)){{$extrainfo->fax}}@endif" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Company/Store</label>
                          <div class="col-lg-9">
                            <input class="form-control company required" name="company" value="@if(!empty($extrainfo->company)){{$extrainfo->company}}@endif" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Website</label>
                          <div class="col-lg-9">
                            <input class="form-control website" name="website" value="@if(!empty($extrainfo->website)){{$extrainfo->website}}@endif" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">ENI (TAX Id)</label>
                          <div class="col-lg-9">
                            <input class="form-control eni required" name="eni" value="@if(!empty($extrainfo->taxid)){{$extrainfo->taxid}}@endif" type="text">
                          </div>
                        </div>
                    </div>
                  </div><!-- dashboardBorder -->

                  <!-- dashboardBorder -->
                  <div class="form-group pdl20">                    
                  <button class="btn btn-primary open-door">Submit</button>
                  </div>
               {{ Form::close() }}                
               </section> 
                    
            </div>
            </div>

       
            

@endsection
