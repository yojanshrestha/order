@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('Order View') }}</small>
    </h1>
@endsection

@section('content')
    <!-- Main content -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Order Details</h3>
            {{-- <div class="box-tools pull-right">
                @include('backend.products.includes.headerbutton')
            </div> --}}<!--box-tools pull-right-->
            <a href="{{url('user/orderlist')}}" class="btn btn-primary alignright" style="float: right;">Back</a>
        </div><!-- /.box-header -->

        <!-- /.box-header -->
        <div class="box-body">
                 {{-- <h4>Order Id: {{$orderview->id}}</h4> --}}
                 <h4>Order Number: {{$orderview->identifier}}</h4>
            <div class="table-responsive">
                <table id="product-table1" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Product Id</th>
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Rate</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($orderview->orderItems as $key => $order)
                            {{-- @php 
                            $name = $orderview->productName($order->product_id);
                            $img = $orderview->productImage($order->product_id);
                            @endphp --}}
                            <tr>
                                @if(empty($order->product))
                                    <td>{{$order->product_id}}</td>
                                    <td>
                                        <img src="" width="50" height="50" alt="-" title="">
                                    </td>
                                    <td>(product deleted)</td>
                                @else
                                    <td>{{$order->product_id}}</td>
                                    <td>
                                        <img src="{{asset('images/products/'.$order->product->feat_img)}}" width="50" height="50" 
                                        alt="{{$order->product->name}}" title="{{$order->product->name}}">
                                    </td>
                                    <td>{{$order->product->name}}</td>
                                @endif
                                <td class="">{{$order->qty}}</td>
                                <td class="">$ {{$order->rate}}</td>
                                <td class="">$ {{$order->qty * $order->rate}}</td>
                               
                            </tr>
                            @endforeach
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Grand Total</strong></td>
                                <td> $
                                    <?php $sum = 0; ?>
                                    @foreach($orderview->orderItems as $key => $order)
                                    <?php 
                                    $sum += $order->qty*$order->rate; 
                                    ?>
                                    @endforeach
                                        {{$sum}}
                                </td>
                            </tr>
                    </tbody>

                </table>
            </div>
            {{-- @include('backend.includes.bulkactionform', ['url'=>'admin/products/bulkdelete']) --}}
 
        </div>
        <!-- /.box-body -->
    </div>

@endsection 