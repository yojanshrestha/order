@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('Product List') }}</small>
    </h1>
@endsection

@section('content')
    <!-- Main content -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List of Products</h3>
            {{-- <div class="box-tools pull-right">
                @include('backend.products.includes.headerbutton')
            </div> --}}<!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <!-- /.box-header -->
        <div class="box-body">
        <form action="{{url('user/productorder/store')}}" method="post" name="productorder" class="productorder">   
        {{csrf_field()}}         
            <div class="table-responsive">
                <table id="product-tabl" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Quantity</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($products as $key => $product)
                        <tr>
                                <td>
                                <label for="checkbox{{$key}}">
                                <input type="checkbox" id="checkbox{{$key}}" class="checkAll11" name="bulk_select[]" value="{{$product->id}}">
                                    </label>
                                </td>
                                <td><input type="number" name="qty[]" min="1" max="{{$product->qty}}" class="form-control qty" value="1" disabled></td>
                                <td>
                                    <img src="{{asset('images/products/'.$product->feat_img)}}" width="50" height="50" 
                                        alt="{{$product->name}}" title="{{$product->name}}">
                                </td>
                                <td>{{$product->name}}</td>
                                <td>{!! $product->description !!}</td>
                                <td class="price-wrap">$ <span class="price"> {{$product->price}} </span></td>
                                <td class="">$ <span class="totalprice">{{$product->price}}</span><input type="hidden" name="totalprice[]" class="form-control totalprice" value="{{$product->price}}" readonly disabled>
                                </td>
                        </tr>
                            @endforeach

                    </tbody>

                </table>
            </div>
            {{-- @include('backend.includes.bulkactionform', ['url'=>'admin/products/bulkdelete']) --}}
        <div>
            <input type="submit" name="submit" value="Save" disabled="disabled" class="btn btn-primary btnsubmit">
        </div>
        </form>
        </div>
        <!-- /.box-body -->
    </div>

@endsection