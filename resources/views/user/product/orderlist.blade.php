@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('Order List') }}</small>
    </h1>
@endsection

@section('content')
    <!-- Main content -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List of Orders</h3>
            {{-- <div class="box-tools pull-right">
                @include('backend.products.includes.headerbutton')
            </div> --}}<!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <!-- /.box-header -->
        <div class="box-body">
                 
            <div class="table-responsive">
                @if(count($orderlist) > 0)
                <table id="product-tables" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Order Number</th>
                            <th>Total Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($orderlist as $key => $order)
                        <tr>
                                <td>{{$order->id}}</td>
                               <td> {{$order->identifier}}</td>
                               <td> $ {{$order->total}}</td>
                                <td class="">{!!$order->OrderStatus!!}</td>
                                <td class="">
                                    <a href="{{url('user/orderlist/show',$order->id)}}" class="btn btn-primary">View</a>
                                    @if($order->status == 'pending')
                                    <a href="{{url('user/orderlist/destroy',$order->id)}}" class="btn btn-primary btn-danger">Cancel</a>
                                    @endif
                                </td>
                        </tr>
                            @endforeach
                            <?php /*
                            <tr>
                                <td></td>
                                <td>Grand Total</td>
                                <td> $
                                    <?php $sum = 0; ?>
                                    @foreach($orderlist as $key => $order)
                                    <?php 
                                    $sum += $order->total; 
                                    ?>
                                    @endforeach
                                        {{$sum}}
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            */ ?>
                    </tbody>

                </table>
                @else
                <div class="not-item">
                    <p>There is no order in the list.</p>
                </div>
                @endif
            </div>
            {{-- @include('backend.includes.bulkactionform', ['url'=>'admin/products/bulkdelete']) --}}
 
        </div>
        <!-- /.box-body -->
    </div>

@endsection