
<footer>
    <div class="container">
        <ul class="footer-menu">
            <li><a href="{{URL::to('/about-us')}}">about us</a></li>
            <li><a href="{{URL::to('/terms-conditions')}}">terms & conditions</a></li>
            <li><a href="{{URL::to('/privacy-policy')}}">privacy policy</a></li>
            <li><a href="{{URL::to('/faqs')}}">FAQs</a></li>
            <li><a href="{{URL::to('/contact-us')}}">contacts</a></li>
        </ul>
        <div class="footer-product-list">
            <div class="footer-products">
            {!!$settings->footer_content!!}
            </div>
        </div>
        <div class="row bottom-footer">
            <div class="col-md-6 col-sm-12"><p class="credit">XYZ. all rights reserved.</p></div>
            <div class="col-md-6 col-sm-12 text-right"><p class="credit">Web Design &nbsp; <a href="http://www.xyz.com/"><img src="{{asset('/footer.png')}}" title="XYZ" alt="XYZ" class="footerimg"></a></p></div>
        </div>
        
    </div>
</footer>
<style>
    .footerimg{
        height: 25px;
        width: 25px;
        background: #fff;
        border-radius: 50%;
        border: 2px solid #f9f9f9;
    }
    .m30{
        margin-top: 30px;
    }
    .product-buynow .nav-tabs .nav-item,
    .product-detail-page .nav-tabs .nav-item{
        margin-bottom: 0;
    }
    .product-buynow .nav-tabs .nav-item a,
    .product-detail-page .nav-tabs .nav-item a{
        font-size: 16px;
        padding-bottom : 10px;
    }
    .product-buynow .nav-link.active,
    .product-detail-page .nav-link.active{
        border-bottom: none;
        border-color: transparent transparent #000 transparent;
        padding: 0;
    }
    .product-buynow .xzoom{
        margin-left: 0;
    }
    .product-detail-page .product-detail{
        border-bottom: none;;
    }
    .product-buynow .tab-content, .product-detail-page .tab-content{
        padding: 20px 0 0 0;
    }

 
</style>