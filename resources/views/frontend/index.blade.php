@extends('frontend/layouts/app')

@section('title'){{$settings->title}}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

    @include('frontend/includes/banner')
    
    <section class="feature-products">
        <div class="container">
            @if(!empty($page->top_content))
                <?php 
                    file_put_contents(base_path() . '/resources/views/frontend/temp.blade.php', $page->top_content);
                    $html = view('frontend.temp')->render();
                    echo $html; 
                ?>
            @endif
            {{-- {!! getCatProd([2, 1, 2], ['c-65', 'c-62', 'p-9', 'c-61', 'p-9']) !!}  --}}
            
            {{-- <div class="advertise-block">
                <div class="adv-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <figure class="bg-cover" 
                            style="background-image: url('{{asset('/images/banner-adv.png')}}');">
                            </figure>
                        </div>
                        <div class="col-sm-8">
                            <div class="adv-text d-flex">
                                <div class="align-self-center">
                                    <p>kurti at unbelievable price</p>
                                    <h2>starting at just rs. 499/-</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

            {{-- {!! getCatProd([1, 2, 2], ['c-65', 'c-62', 'p-9', 'c-61', 'p-9']) !!}  --}}
            
            {{-- <div class="advertise-block">
                <div class="adv-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <figure class="bg-cover" 
                            style="background-image: url('{{asset('/images/banner-adv.png')}}');">
                            </figure>
                        </div>
                        <div class="col-sm-8">
                            <div class="adv-text d-flex">
                                <div class="align-self-center">
                                    <p>kurti at unbelievable price</p>
                                    <h2>starting at just rs. 499/-</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            
            {{-- {!! getCatProd([2, 2, 1], ['c-65', 'c-62', 'p-9', 'c-61', 'p-9']) !!}  --}}
            
            {{-- <div class="lg-button-group text-center mt30">
                <a href="#" class="btn btn-lg btn-lg-blue-o">
                    more
                </a>
            </div> --}}
        </div>
    </section>
    <section class="about text-center">
        <div class="container">
            <div class="about-desc">
                <h4>About npwears</h4>
                <p>npwears is a marketplace for designer merchandise created by fantastic independent artists from all over the world. We connect art with lifestyle by enabling you to shop directly from creators. Browse and shop thousands of inspring designs accross a range of quality products.</p>
            </div>
        </div>
    </section>
@endsection