<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
    <title>@yield('title')@if(!empty($settings->tagline)) | {{$settings->tagline}}@endif</title>
    {{-- SEO --}}
    <meta name="meta_title" content="@yield('meta_title')">
    <meta name="meta_description" content="@yield('meta_description')">
    <meta name="meta_keyword" content="@yield('meta_keyword')">
    {{-- Social Sharing Facebook--}}
    <meta property="og:title" content="@yield('og_title')" />
    <meta property="og:url" content="@yield('og_url')" />
    <meta property="og:image" content="@yield('og_image')" />
    <meta property="og:description" content="@yield('og_description')" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable = no">
    <link rel="shortcut icon" href="@if(!empty($settings->favicon)){{asset('/images/logo/favicon/'.$settings->favicon)}}@endif">
    <link rel="apple-touch-icon" href="@if(!empty($settings->favicon)){{asset('/images/logo/favicon/'.$settings->favicon)}}@endif">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/frontend/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/frontend/ap-drilldown-menu.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/frontend/owl.carousel.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/frontend/nprogress.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/frontend/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/frontend/responsive.css') }}">
    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
    </script>
</head>

    <body class="{{ !empty($class) ? $class : '' }}">
        {{csrf_field()}}
        @include('frontend/includes/header')
        @yield('content')
        @include('frontend/includes/socialinfo')
        @include('frontend/includes/footer')

        <div class="loader" style="display: none;">
            <img src="{{asset('/images/processing.gif')}}" width="50" height="50">
        </div>
      
        {{-- Javascripts --}}
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
        <script type="text/javascript" src="{{ asset('/js/frontend/jquery-3.3.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/backend/jquery.validate.js') }}"></script>
        {{-- <script type="text/javascript" src="{{ asset('/js/frontend/bootstrap.js') }}"></script>    --}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/owl.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/ap-drilldown-menu.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/zoom-setup.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/xzoom.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/nprogress.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/script.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/custom.js') }}"></script>
        <script type="text/javascript">
          function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
            document.body.className += ' ' + 'home';
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.body.className = document.body.className.replace("home","");

        }
        </script>
         
        <script type="text/javascript">
          $(function() {
            $('#menu').apDrillDownMenu({
            });
          });
        </script>
    </body>
</html>