<!DOCTYPE html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Register  </title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Dog Chew">
<meta name="author" content="Dog Chew">

{{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
{{ Html::style(mix('css/frontend.css')) }}

<!-- iCheck -->
{!! Html::style('css/orange.css') !!}

{!! Html::style('css/login.css') !!}

</head>

<body class="hold-transition login-page">
	
	<div class="register-box">
		

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.
			<ul class="list-unstyled">
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<div class="login-box-body">
			<!-- <p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }} </p> -->
			<form action="{{ url('/user/register') }}" method="post" class="registerform">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group has-feedback">
					<label>Username <span>*</span></label>
					<input type="text" class="form-control required" name="name"/>
				</div>
				
				<div class="form-group has-feedback">
					<label>Email Address <span>*</span></label>
					<input type="email" class="form-control required" name="email"/>
				</div>
				<div class="form-group has-feedback">
					<!--             <span class="glyphicon glyphicon-lock"></span> -->
					<label>Password <span>*</span></label>
					<!-- <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/> -->
					<input type="password" class="form-control required" name="password"/>
				</div>
				<div class="form-group has-feedback">
					<!--             <span class="glyphicon glyphicon-lock"></span> -->
					<label>Confirm Password <span>*</span></label>
					<!-- <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/> -->
					<input type="password" class="form-control required" name="password_confirmation"/>
				</div>
				<div class="form-group has-feedback">
					<label>Address <span>*</span></label>
					<input type="text" class="form-control required" name="address"/>
				</div>
				<div class="form-group has-feedback">
					<label>APT/STE <span>*</span></label>
					<input type="text" class="form-control required" name="apt"/>
				</div>
				<div class="form-group has-feedback">
					<label>City <span>*</span></label>
					<input type="text" class="form-control required" name="city"/>
				</div>
				<div class="form-group has-feedback">
					<label>State <span>*</span></label>
					<input type="text" class="form-control required" name="state"/>
				</div>
				<div class="form-group has-feedback">
					<label>Phone <span>*</span></label>
					<input type="text" class="form-control required" name="phone"/>
				</div>
				<div class="form-group has-feedback">
					<label>Fax</label>
					<input type="text" class="form-control" name="fax"/>
				</div>
				<div class="form-group has-feedback">
					<label>Company/Store <span>*</span></label>
					<input type="text" name="company" value="" class="form-control required">
				</div>
				<div class="form-group has-feedback">
					<label>Website</label>
					<input type="text" name="website" value="" class="form-control">
				</div>
				<div class="form-group has-feedback">
					<label>ENI(Tax ID) <span>*</span></label>
					<input type="text" name="eni" value="" class="form-control required">
				</div>
				<div class="row">
					
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button>
					</div><!-- /.col -->
				</div>
				<input type="hidden" name="backend" value="1">
			</form>
<div>Already have account. <a href="{{url('user/login')}}">Login Here</a></div>

		</div><!-- /.login-box-body -->

	</div><!-- /.login-box -->

	{!! Html::script(mix('js/frontend.js')) !!}
	
	<script src="{{ asset('AdminLTE-master/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/backend/jquery.validate.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/classie.js') }}" type="text/javascript"></script>
	
	<script>
		$(function () {
			$('.registerform').validate()
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-orange',
				radioClass: 'iradio_square-orange',
                increaseArea: '20%' // optional
            });
		});
	</script>

	<script>
		jQuery(document).ready(function($) {

			if (!String.prototype.trim) {
				(function() {
              // Make sure we trim BOM and NBSP
              var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
              String.prototype.trim = function() {
              	return this.replace(rtrim, '');
              };
          })();
      }

      [].slice.call( document.querySelectorAll( '.form-control' ) ).forEach( function( inputEl ) {
             // in case the input is already filled..
             
             if( inputEl.value.trim() !== '' ) {
             	classie.add( inputEl.parentNode, 'open' );
             }

             // events:
             inputEl.addEventListener( 'focus', onInputFocus );
             inputEl.addEventListener( 'blur', onInputBlur );
         } );

      function onInputFocus( ev ) {
      	classie.add( ev.target.parentNode, 'open' );
      }

      function onInputBlur( ev ) {
      	if( ev.target.value.trim() === '' ) {
      		classie.remove( ev.target.parentNode, 'open' );
      	}
      }


  });

</script>
</body>
</html>