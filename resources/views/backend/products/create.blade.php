@extends('backend/layouts/app') 
@section ('title', 'Create Product') 
@section('page-header')
	<h1>
		Add New Product
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Products</a></li>
	    <li class="active">
	        Add New Product
	    </li>
	</ol>
	@endsection 
	@section('content') 
	{{Form::open(['url'=>'user/products/store', 'id'=>'productForm', 'files'=>true])}}
		<div class="box box-orange">
		    <div class="box-header with-border">
		        <h3 class="box-title">General</h3>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
		        <div class="col-md-12">
		            <div class="form-group">
		                <label class="control-label">Name<em class="asterisk">*</em></label>
		                {{Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Enter Product Name'])}}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">SKU<em class="asterisk">*</em></label>
		                {{Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'Enter Unique Sku'])}}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Url</label>
		                {{Form::text('url',null,['class'=>'form-control', 'placeholder'=>'Enter url'])}}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Status<em class="asterisk">*</em></label>
		                {{Form::select('status',['0' => 'Inactive', '1'=>'Active'],null,['class'=>'form-control'])}}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Price<em class="asterisk">*</em></label>
		            	<div class="input-group">
							<span class="input-group-addon">{{'$'}}</span>
			                {{ Form::input('number', 'price', null, ['step'=>'any', 'min'=>'0', 'placeholder'=>'Enter Price', 'class' => 'form-control']) }}
						</div>
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Inventory<em class="asterisk">*</em></label>
		                {{Form::select('inventory',['in stock' => 'In Stock', 'out of stock'=>'Out of Stock'],null,['class'=>'form-control'])}}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Quantity<em class="asterisk">*</em></label>
		                {!! Form::input('number', 'qty', null, ['step'=>'1', 'min'=>'0', 'placeholder'=>'Enter Stock Quantity', 'class' => 'form-control']) !!}
		            </div>
		        </div>
		        <div class="col-md-6">
		            <div class="form-group">
		                <label class="control-label">Featured Image</label>
		                {{Form::file('feat_img',['class'=>'feat-img'])}}
		                <img class="img-center" width="250" height="200" id="img_preview" src="#" alt=""> 
		            </div>
		        </div>
		        <div class="col-md-12">
		            <div class="form-group">
		                <label class="control-label">Description</label>
		                {!!Form::textarea('description',null,['class'=>'form-control', 'rows'=>'8', 'placeholder'=>'Enter Short Description'])!!}
		            </div>
		        </div>
		    </div>
		    <!-- /.box-body -->
		</div>
		<div class="form-group">
		    {{Form::submit('Save',['class'=>'btn btn-karm', 'placeholder'=>'Enter Tags'])}}
		</div>
	{{Form::close()}} 
	@include('backend.includes.tinymce')
@endsection