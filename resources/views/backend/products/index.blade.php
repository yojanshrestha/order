@extends('backend.layouts.app')

@section ('title', 'Products Management')

@section('page-header')
<h1>
	Products Management 
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">
		Products
	</li>
</ol>
@endsection
@section('content')
	<!-- Main content -->
	<div class="box">
		<div class="box-header with-border">
            <h3 class="box-title">List of Products</h3>
            {{-- <div class="box-tools pull-right">
                @include('backend.products.includes.headerbutton')
            </div> --}}
            <!--box-tools pull-right-->
        </div><!-- /.box-header -->

		<!-- /.box-header -->
		<div class="box-body">
            <div class="table-responsive">
            	@if(count($products) > 0)
				<table id="product-tables" class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Image</th>
							<th>Name</th>
							<th>Sku</th>
							<th>Price</th>
							<th>Created At</th>
							<th>Last Updated</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
							@foreach ($products as $product)
						<tr>
								<td>{{$product->id}}</td>
								<td>
									<img src="{{asset('images/products/'.$product->feat_img)}}" width="50" height="50" alt="{{$product->name}}" title="{{$product->name}}">
								</td>
								<td>{{$product->name}}</td>
								<td>{{$product->slug}}</td>
								<td>$ {{$product->price}}</td>
								<td>{{$product->created_at}}</td>
								<td>{{$product->updated_at}}</td>
								<td>{!!$product->statuslabel!!}</td>
								<td>{!!crudOps('products', $product->id)!!}</td>
						</tr>
							@endforeach
					</tbody>
				</table>
				@else
				<div class="no-item">
					<p>There is no product in the list.</p>
				</div>
				@endif
			</div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection
	