@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.edit') }}</small>
    </h1>
@endsection
@php 
$profiles  = DB::table('profiles')->where('user_id',$user->id)->first();
@endphp
@section('content')
    {{ Form::model($user, ['route' => ['admin.access.user.update', $user], 'class' => 'form-horizontal userregister', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.access.users.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.access.includes.partials.user-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-10">
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                @if ($user->id != 1)
                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{Form::checkbox('status','1', true, ['id'=>'checkbox1'])}}<label for="checkbox1"><span></span></label>
                            {{-- {{ Form::checkbox('status', '1', $user->status == 1) }} --}}
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-1">
                            {{Form::checkbox('confirmed','1', true, ['id'=>'checkbox2'])}}<label for="checkbox2"><span></span></label>
                            {{-- {{ Form::checkbox('confirmed', '1', $user->confirmed == 1) }} --}}
                        </div><!--col-lg-1-->
                    </div><!--form control-->
                    {{-- @if(!empty($profiles))     --}}
                    <div class="form-group">
                          <label class="col-lg-2 control-label">Contact address <span>*</span></label>
                            <div class="col-lg-10">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="address" value="@if(!empty($profiles->address)){{$profiles->address}}@endif" type="text" placeholder="address">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">APT/STE <span>*</span></label>
                            <div class="col-lg-10">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="apt" value="@if(!empty($profiles->apt)){{$profiles->apt}}@endif" type="text" placeholder="apt/ste">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">City <span>*</span></label>
                            <div class="col-lg-10">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="city" value="@if(!empty($profiles->city)){{$profiles->city}}@endif" type="text" placeholder="city">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">State <span>*</span></label>
                            <div class="col-lg-10">
                              <div class="user-formcontrol">
                                <input class="col-lg-10 form-control required" name="state" value="@if(!empty($profiles->state)){{$profiles->state}}@endif" type="text" placeholder="state">
                              </div>
                              
                            </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Phone <span>*</span></label>
                          <div class="col-lg-10">
                            <input class="form-control phone required" name="phone" value="@if(!empty($profiles->phone)){{$profiles->phone}}@endif" type="text" placeholder="phone number">
                          </div>
                        </div>
                      <div class="form-group">
                          <label class="col-lg-2 control-label">Fax </label>
                          <div class="col-lg-10">
                            <input class="form-control fax" name="fax" value="@if(!empty($profiles->fax)){{$profiles->fax}}@endif" type="text" placeholder="fax number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Company/Store <span>*</span></label>
                          <div class="col-lg-10">
                            <input class="form-control company required" name="company" value="@if(!empty($profiles->company)){{$profiles->company}}@endif" type="text" placeholder="company/store name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Website</label>
                          <div class="col-lg-10">
                            <input class="form-control website" name="website" value="@if(!empty($profiles->website)){{$profiles->website}}@endif" type="text" placeholder="http://www.example.com">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">ENI (TAX Id) <span>*</span></label>
                          <div class="col-lg-10">
                            <input class="form-control eni required" name="eni" value="@if(!empty($profiles->taxid)){{$profiles->taxid}}@endif" type="text" placeholder="Tax Id">
                          </div>
                        </div>
                       {{-- @endif  --}}

                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-3">
                            @if (count($roles) > 0)
                                @foreach($roles as $role)
                                 @if($role->name != 'Administrator')
                                    <input type="checkbox" value="{{$role->id}}" name="assignees_roles[{{ $role->id }}]" {{ is_array(old('assignees_roles')) ? (in_array($role->id, old('assignees_roles')) ? 'checked' : '') : (in_array($role->id, $user_roles) ? 'checked' : '') }} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}"><span></span>{{ $role->name }}</label>
                                        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                            (
                                                <span class="show-text">{{ trans('labels.general.show') }}</span>
                                                <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                                {{ trans('labels.backend.access.users.permissions') }}
                                            )
                                        </a>
                                    @endif
                                    <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                        @if ($role->all)
                                            {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                        @else
                                            @if (count($role->permissions) > 0)
                                                <blockquote class="small">{{--
                                            --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                    @endforeach
                                                </blockquote>
                                            @else
                                                {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                            @endif
                                        @endif
                                    </div><!--permission list-->
                                @endforeach
                            @else
                                {{ trans('labels.backend.access.users.no_roles') }}
                            @endif
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                @endif
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        @if ($user->id == 1)
            {{ Form::hidden('status', 1) }}
            {{ Form::hidden('confirmed', 1) }}
            {{ Form::hidden('assignees_roles[]', 1) }}
        @endif

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}
@endsection
