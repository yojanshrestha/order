@php
$profile = DB::table('profiles')->where('user_id',$user->id)->first();
@endphp
<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.avatar') }}</th>
        <td><img src="{{ $user->picture }}" class="user-profile-image" /></td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.name') }}</th>
        <td>{{ $user->name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.email') }}</th>
        <td>{{ $user->email }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.status') }}</th>
        <td>{!! $user->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.confirmed') }}</th>
        <td>{!! $user->confirmed_label !!}</td>
    </tr>

    <tr>
        <th>Address</th>
        <td>{{$profile->address}}</td>
    </tr>
    <tr>
        <th>APT/STE</th>
        <td>{{$profile->apt}}</td>
    </tr>
    <tr>
        <th>City</th>
        <td>{{$profile->city}}</td>
    </tr>
<tr>
        <th>State</th>
        <td>{{$profile->state}}</td>
    </tr>
    <tr>
        <th>Phone</th>
        <td>{{$profile->phone}}</td>
    </tr>
    <tr>
        <th>Fax</th>
        <td>{{$profile->fax}}</td>
    </tr>
    <tr>
        <th>Company/Store</th>
        <td>{{$profile->company}}</td>
    </tr>
    <tr>
        <th>Website</th>
        <td>{{$profile->website}}</td>
    </tr>
    <tr>
        <th>Tax Id</th>
        <td>{{$profile->taxid}}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.created_at') }}</th>
        <td>{{ $user->created_at }} ({{ $user->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.last_updated') }}</th>
        <td>{{ $user->updated_at }} ({{ $user->updated_at->diffForHumans() }})</td>
    </tr>

    @if ($user->trashed())
        <tr>
            <th>{{ trans('labels.backend.access.users.tabs.content.overview.deleted_at') }}</th>
            <td>{{ $user->deleted_at }} ({{ $user->deleted_at->diffForHumans() }})</td>
        </tr>
    @endif
</table>