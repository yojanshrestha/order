<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ active_class(Active::checkUriPattern('user/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>
        @if(!access()->hasRole('Administrator'))
           @role(2)
                <li class="{{ active_class(Active::checkUriPattern(['user/productorder*', 'user/orderlist*'])) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Product</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern(['user/productorder*', 'user/orderlist*']), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern(['user/productorder*', 'user/orderlist*']), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('user/productorder*')) }}">
                        <a href="{{ route('admin.productorder.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Order Product</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('user/orderlist*')) }}">
                        <a href="{{ route('admin.orderlist.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Order List</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('user/user/profile')) }} treeview">
                <a href="{{ route('admin.profile') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>My Profile</span>
                        </a>
            </li>
           @endauth
        @endif
            @permission('view-products-management')
                <li class="{{ active_class(Active::checkUriPattern('user/products*')) }} treeview">
                  <a href="#">
                    <i class="fa fa-list"></i><span>Product Manangement</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu {{ active_class(Active::checkUriPattern('user/products*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('user/products*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('user/products/create')) }}">
                      <a href="{!! url('user/products/create') !!}"><i class="fa fa-plus"></i> Create</a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('user/products')) }}">
                      <a href="{!! url('user/products') !!}"><i class="fa fa-file-text-o"></i> All Products</a>
                    </li>
                  </ul>
                </li>
            @endauth

            @permission('view-orders-management')
                <li class="{{ active_class(Active::checkUriPattern('user/orders*')) }} treeview">
                  <a href="{!! url('user/orders') !!}">
                    <i class="fa fa-list"></i><span>Order Manangement</span>
                  </a>
                </li>
            @endauth

            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('user/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('user/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('user/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('user/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>

                    {{-- <li class="{{ active_class(Active::checkUriPattern('user/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li> --}}
                </ul>
            </li>
            @endauth

        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>