@extends('backend.layouts.app')

@section ('title', 'Orders Management')

@section('page-header')
<h1>
	Orders Management 
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">
		Orders
	</li>
</ol>
@endsection
@section('content')
	<!-- Main content -->
	<div class="box">
		<div class="box-header with-border">
            <h3 class="box-title">List of Orders</h3>

            <div class="box-tools pull-right">
                <div class="pull-right mb-10 hidden-sm hidden-xs">
				    {{ link_to_route('admin.orders', 'All Orders', [], ['class' => 'btn btn-info btn-xs']) }}
				    {{ link_to_route('admin.orders', 'Pending Orders', ['status'=>'pending'], ['class' => 'btn btn-warning btn-xs']) }}
				    {{ link_to_route('admin.orders', 'Verified Orders', ['status'=>'verified'], ['class' => 'btn btn-primary btn-xs']) }}
				    {{ link_to_route('admin.orders', 'Completed Orders', ['status'=>'completed'], ['class' => 'btn btn-success btn-xs']) }}
				    {{ link_to_route('admin.orders', 'Cancelled Orders', ['status'=>'cancelled'], ['class' => 'btn btn-danger btn-xs']) }}
				</div><!--pull right-->

				<div class="pull-right mb-10 hidden-lg hidden-md">
				    <div class="btn-group">
				        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				            Orders <span class="caret"></span>
				        </button>

				        <ul class="dropdown-menu" role="menu">
				            <li>{{ link_to_route('admin.orders', 'All Orders', []) }}</li>
				            <li>{{ link_to_route('admin.orders', 'Pending Orders', ['status'=>'pending']) }}</li>
				            <li>{{ link_to_route('admin.orders', 'Verified Orders', ['status'=>'verified']) }}</li>
				            <li>{{ link_to_route('admin.orders', 'Completed Orders', ['status'=>'completed']) }}</li>
				            <li>{{ link_to_route('admin.orders', 'Cancelled Orders', ['status'=>'cancelled']) }}</li>
				        </ul>
				    </div><!--btn group-->
				</div><!--pull right-->

				<div class="clearfix"></div>
            </div><!--box-tools pull-right-->

        </div><!-- /.box-header -->
		<!-- /.box-header -->
		<div class="box-body">
            <div class="table-responsive">
            	@if(count($orders) > 0)
				<table id="product-tables" class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Order Number</th>
							<th>Username</th>
							<th>Total Amount</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $key => $order)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$order->identifier}}</td>
								<td>{{ (!empty($order->user))? $order->user->name : $order->user_id . ' (user deleted)'}}</td>
								<td>$ {{$order->total}}</td>
								<td>{!!$order->OrderStatus!!}</td>
								<td><a href="{{url('user/order/show',$order->id)}}" class="btn btn-info">View</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
				@else
				<div class="no-items">
					<p>There is no order in the list.
				</div>
				@endif
			</div>
			{{-- @include('backend.includes.bulkactionform', ['url'=>'admin/products/bulkdelete']) --}}
		</div>
		<!-- /.box-body -->
	</div>
@endsection
	