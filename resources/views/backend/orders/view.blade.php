@extends('backend.layouts.app') 

@section ('title', 'Orders Management')

@section('page-header')
<h1>
    {{ app_name() }}
</h1> 
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"><i class="fa fa-dashboard"></i> Order</a></li>
    <li class="active">
        View
    </li>
</ol>
@endsection 

@section('content')
<!-- Main content -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Order Details</h3> 
        <a href="{{url('user/orders')}}" class="btn btn-primary">Back</a>
    </div>
    <div class="box-body">
        <h4>Order Number: {{$vieworder->identifier}}</h4>
        <div class="table-responsive">
            <table id="" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Rate</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $sum = 0; ?>
                    @foreach ($vieworder->orderItems as $key => $order) 
                        <tr>
                            @if(empty($order->product))
                                <td>{{$order->product_id}}</td>
                                <td>
                                    <img src="" width="50" height="50" alt="-" title="">
                                </td>
                                <td>(product deleted)</td>
                            @else
                                <td>{{$order->product_id}}</td>
                                <td>
                                    <img src="{{asset('images/products/'.$order->product->feat_img)}}" width="50" height="50" 
                                        alt="{{$order->product->name}}" title="{{$order->product->name}}">
                                </td>
                                <td>{{$order->product->name}}</td>
                            @endif
                            <td>{{$order->qty}}</td>
                            <td>$ {{$order->rate}}</td>
                            <td>$ {{$order->qty * $order->rate}}</td>
                            <?php $sum += $order->qty * $order->rate; ?>
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Grand Total</strong></td>
                        <td>$ {{$sum}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="box-tools pull-right">
                <div class="pull-right mb-10 hidden-sm hidden-xs">
                    @if ($vieworder->status == 'pending')
                        <a href="{{url('user/order/verified',$vieworder->id)}}" onClick="return confirm('Are you sure want to verify?')" class="btn btn-primary">Verify</a>
                    @endif
                    @if ($vieworder->status == 'verified')
                        <a href="{{url('user/order/completed',$vieworder->id)}}" onClick="return confirm('Are you sure want to complete?')" class="btn btn-success">Complete</a>
                    @endif
                    @if ($vieworder->status == 'pending' || $vieworder->status == 'verified')
                        <a href="{{url('user/order/cancelled',$vieworder->id)}}" onClick="return confirm('Are you sure want to cancel?')" class="btn btn-danger">Cancel</a>
                    @endif
                </div>
        </div><!--box-tools pull-right-->
    </div>
</div>
@endsection