<div style="margin: 0 auto; padding: 50px 0; width: 100%;"><center>
<table style="width: 600px; margin: 0px auto; background: #fff; padding: 0px; border: 1px solid #ececec;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="logo">
<td style="padding: 0 20px 10px; border-bottom: 1px dashed #500847; margin: 0;"><a style="display: block;" href="{{url('/')}}"> <img class="w320" src="http://www.gravatar.com/avatar/edf57c7ecaf6cc768f5255007cd17cba.jpg?s=80&d=mm&r=g" alt="company logo" height="100" /> </a></td>
</tr>
<tr class="main-content" style="padding: 0; margin: 0;">
<td style="font-size: 14px; padding: 20px 20px 0px; font-weight: 600; font-family: Arial; margin-top: 10px;">
<p style="padding: 0 0 5px 0; margin: 0;">Hello Admin,<br /><br /></p>
</td>
</tr>
<tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">New User has been registered. Please find the information:</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Username:</b> {{$data['name']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Email:</b> {{$user['email']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Address:</b> {{$data['address']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>APT/STE:</b> {{$data['apt']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>City:</b> {{$data['city']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>State:</b> {{$data['state']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Phone:</b> {{$data['phone']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Fax:</b> {{$data['fax']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Company/Store:</b> {{$data['company']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Website:</b> {{$data['website']}}</p>
<p style="padding: 0 0 5px 0; margin: 0;"><b>Tax Id:</b> {{$data['eni']}}</p>
</td>
</tr>
</tbody>
</table>
</center></div>