<?php
/*
* Orders Management
*/
Route::group([
	'middleware' => 'access.routeNeedsPermission:view-orders-management',
], function() {
	Route::get('/orders/', 'OrderController@index')->name('orders');
	Route::get('/order/show/{id}', 'OrderController@show')->name('order.show');
	Route::get('/order/{status}/{id}', 'OrderController@update')->name('order.update');

});