<?php
/*
* Product Management
*/
Route::group([
	'middleware' => 'access.routeNeedsPermission:view-products-management',
], function() {
	Route::get('/products/create', 'ProductController@create')->name('create.product');
	Route::post('/products/store', 'ProductController@store')->name('store.product');
	Route::get('/products', 'ProductController@index')->name('list.products');
	Route::get('/products/{id}/edit', 'ProductController@edit')->name('products.edit');
	Route::patch('/products/{id}/update', 'ProductController@update')->name('products.update');
	Route::delete('/products/{id}/destroy', 'ProductController@destroy')->name('products.destroy');

});