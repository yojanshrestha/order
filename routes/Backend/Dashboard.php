<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('productorder/store', 'ProductOrderController@store');
Route::resource('productorder', 'ProductOrderController');
Route::get('orderlist/show/{id}', 'OrderListController@show');
Route::get('orderlist/destroy/{id}', 'OrderListController@destroy');
Route::resource('orderlist', 'OrderListController');

Route::get('user/profile', 'DashboardController@profile')->name('profile');
Route::patch('profile/update/{id}', 'ProfileController@userUpdate')->name('profile.update');
Route::patch('password/change', 'ChangePasswordController@changePassword')->name('password.change');
Route::get('user/password', 'ProfileController@formPassword')->name('password');

// Route::patch('password/update/{id}', 'ProfileController@passwordUpdate')->name('password.update');